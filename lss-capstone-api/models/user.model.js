const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const User = new Schema(
  {
    _id: { type: Number },
    sid: { type: String },
    email: { type: String, required: true },
    password: { type: String, require: true },
    firstname: { type: String },
    lastname: { type: String },
    phone: { type: String }, // <- change to optional
    roles: { type: [Number] }
  },
  {
    collection: "users"
  }
);

module.exports = mongoose.model("User", User);
