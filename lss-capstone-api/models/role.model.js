const mongoose = require("Mongoose");
const Schema = mongoose.Schema;

const Role = new Schema(
  {
    _id: { type: Number },
    sid: { type: String },
    desc: { type: String },
    services: { type: [Number] }
  },
  { collection: "roles" }
);

module.exports = mongoose.model("Role", Role);
