const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const Field = require("./field.model");

const Service = new Schema(
  {
    _id: { type: Number },
    sid: { type: String },
    desc: { type: String },
    theme: { type: String },
    booking_type: { type: String },
    fields: { type: [Field] },
    sched: { type: [Schedule] }
  },
  { collection: "services" }
);

module.exports = mongoose.model("Service", Service);
