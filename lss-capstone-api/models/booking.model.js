const mongoose = require("mongoose");
const Schema = mongoose.Schema;

let Booking = new Schema(
  {
    _id: { type: mongoose.Types.ObjectId },
    service_id: { type: Number, required: true },
    book_type: { type: String, required: true }, // should be either appt (appointment) or walkin (walk-in).
    fname: { type: String },
    lname: { type: String },
    phone: { type: String },
    email: { type: String, required: true },
    course: { type: String },
    start: { type: String, required: true },
    end: { type: String, required: true }
  },
  { collection: "bookings" }
);

module.exports = mongoose.model("Booking", Booking);
