const bodyparser = require("body-parser");
const express = require("express");
const fs = require("fs");
const jwt = require("../modules/token.js");
const config = require("../config/config.json");

module.exports = function() {
    var api = express.Router();

    api.use(bodyparser.urlencoded({ extended: false }));

    // Endpoint.
    api.post("/role", function(request, response) {
        (async function() {
            let roles = [];

            try {
                roles = JSON.parse(
                    fs.readFileSync("./dummy/roles.json", "utf-8")
                );
            } catch (error) {
                console.log(error);
            }

            let code = request.body.code;
            let desc = request.body.desc;
            let privs = request.body.privileges;
            let services = request.body.services;

            const role = {
                code: code,
                description: desc,
                privileges: privs,
                services: services
            };

            roles.push(role);
            fs.writeFileSync("./dummy/roles.json", JSON.stringify(roles));

            // No JWT for now.
            const json = {
                success: true,
                message: "Role saved."
            };

            response.status(200).send(JSON.stringify(json));
        })();
    });

    // Endpoint for service.
    api.get("/role", function(request, response) {
        (async function() {
            // Read from mock database.
            let roles = [];

            try {
                roles = JSON.parse(
                    fs.readFileSync("./dummy/roles.json", "utf-8")
                );
            } catch (error) {
                console.log(error);
            }

            //console.log(roles);

            // Look from code from services' table.
            const code = request.query.r;

            //console.log(code);

            if (code) {
                const role = roles.find(
                    i => i.code.toLowerCase() === code.toLowerCase()
                );

                const json = {
                    success: true,
                    message: "success",
                    result: role
                };

                response.status(200).send(JSON.stringify(json));
            } else {
                const json = {
                    success: true,
                    message: "success",
                    result: roles
                };

                response.status(200).send(JSON.stringify(json));
            }
        })();
    });

    return api;
};
