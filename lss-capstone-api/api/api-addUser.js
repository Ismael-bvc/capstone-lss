const bodyparser = require("body-parser");
const express = require("express");
const fs = require("fs");
const jwt = require("../modules/token.js");
const config = require("../config/config.json");
const bcrypt = require("bcrypt");
const mailgun = require("mailgun-js");

module.exports = function() {
  var api = express.Router();

  api.use(bodyparser.urlencoded({ extended: false }));

  // Endpoint for login.
  api.post("/addUser", function(request, response) {
    (async function() {
      // Read from mock database.
      const email = request.body.email;
      body = request.body;

      const DOMAIN = "sandbox438c9da22c0f40bcb5f21b55e7396ba4.mailgun.org";
      const mg = mailgun({
        apiKey: "394cdfc59cd51c3a343fb9cd120ee3eb-9dda225e-281818e6",
        domain: DOMAIN
      });
      const data = {
        from: "gogoidipti82@gmail.com",
        to: email,
        subject: "Hello",
        text: "Invitation to accept as  a tutor "
      };
      mg.messages().send(data, function(error, body) {
        console.log("msg", body);
      });

      const json = {
        success: true,
        message: "Mail sent to tutor successful"
      };

      response.status(200).send(JSON.stringify(json));
    })();
  });

  return api;
};
