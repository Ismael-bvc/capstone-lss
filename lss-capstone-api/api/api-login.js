const bodyparser = require("body-parser");
const express = require("express");
const fs = require("fs");
const jwt = require("../modules/token.js");
const config = require("../config/config.json");
const bcrypt = require("bcrypt");
const cors = require("cors");
const mongoose = require("mongoose");

//
// Importing User model.
const User = require("../models/user.model");

//
// Import Login model.
const Login = require("../models/login.model");

module.exports = function() {
  var api = express.Router();

  api.use(bodyparser.urlencoded({ extended: false }));

  //
  // Endpoint for login.
  api.post("/login", function(request, response) {
    (async function() {
      //
      // Read from MongoDB Atlas Database.
      const USERNAME = config.MongoDbAtlasUsername;
      const PASSWORD = config.MongoDbAtlasPassword;
      const DB_NAME = config.MongoDbAtlasDatabaseName;
      const URI =
        "mongodb+srv://" +
        USERNAME +
        ":" +
        PASSWORD +
        "@lss-cluster-zk9jl.mongodb.net/" +
        DB_NAME +
        "?retryWrites=true&w=majority";

      //
      // Make a connection to Atlas. The second argument is optional. It is just there to get rid of a warning
      // that you receive when you run the server.
      mongoose.connect(URI, {
        useNewUrlParser: true,
        useUnifiedTopology: true
      });

      const email = request.body.username.toLowerCase();
      const password = request.body.password;

      try {
        let isUser, lssUser, credentials, credential;
        //
        // The 'User' model has a built-in static method called 'find'. If you open the file for User
        // it will not explicitly show a 'find' method. Every Mongoose model has available to it a certain
        // set of static methods; 'find' being one of them :)
        await User.find({}, (err, users) => {
          users.forEach(user => {
            if (user.email === email) {
              isUser = true;
              lssUser = user;
            }
          });
        });

        //
        // We should build it from the keys and other tables but for easier process,
        //      just put it in login.json and find email from there.
        if (isUser && bcrypt.compareSync(password, lssUser.password)) {
          credentials = await Login.find({});

          credential = credentials.find(cred => cred.sub === lssUser.email);

          const payload = Object.assign(credential, {
            iat: new Date().getTime()
          });

          const token = jwt.createToken(payload, config.LoginSecret);

          const json = {
            success: true,
            message: "success",
            token: token
          };

          response.status(200).send(JSON.stringify(json));
        } else {
          response.status(200).send(
            JSON.stringify({
              success: false,
              message: "Invalid email/password"
            })
          );
        }
      } catch (e) {
        console.error(e);
      }
    })();
  });

  // Endpoint for forgot password.
  api.post("/forgot", function(request, response) {
    (async function() {
      // Validate first if user exists.
      let users = [];

      try {
        users = JSON.parse(fs.readFileSync("./dummy/users.json", "utf-8"));
      } catch (error) {
        console.log(error);
      }

      // Look from username and password in users' table.
      const user = request.body.username;
      const login = users.find(i => i.sub.toLowerCase() === user.toLowerCase());

      if (login) {
        // Create token.
        const payload = {
          sub: login.sub,
          iat: new Date().getTime()
        };
        const token = jwt.createToken(payload, config.ForgotSecret);

        response.status(200).send(
          JSON.stringify({
            success: true,
            message: "",
            token: token
          })
        );
      } else {
        response.status(200).send(
          JSON.stringify({
            success: false,
            message: "Invalid username."
          })
        );
      }
    })();
  });

  // Endpoint for reset password.
  api.post("/reset", function(request, response) {
    (async function() {
      // Get token and password value.
      const password = request.body.password;
      const token = request.body.token;

      // Validate token.
      const valid = jwt.isValidToken(token, config.ForgotSecret);

      if (valid) {
        const payload = jwt.getPayload(token);

        // Check if token already used before.
        let resettokens = [];

        try {
          resettokens = JSON.parse(
            fs.readFileSync("./dummy/resettokens.json", "utf-8")
          );
        } catch (error) {
          console.log(error);
        }

        // Check if it is not already used.
        if (!resettokens.includes(token)) {
          // Add token to list of used tokens.
          resettokens.push(token);
          fs.writeFileSync(
            "./dummy/resettokens.json",
            JSON.stringify(resettokens)
          );

          // Check if token already expired.
          if (new Date().getTime() < Number(payload.iat) + 600000) {
            // Look for user.
            let users = [];

            try {
              users = JSON.parse(
                fs.readFileSync("./dummy/users.json", "utf-8")
              );
            } catch (error) {
              console.log(error);
            }

            const login = users.find(
              i => i.sub.toLowerCase() === payload.sub.toLowerCase()
            );

            if (login) {
              const hash = bcrypt.hashSync(password, config.SaltRounds);

              for (let i = 0; i < users.length; i++) {
                if (users[i].sub === login.sub && users[i]._id === login._id) {
                  users[i].password = hash;
                  break;
                }
              }

              fs.writeFileSync("./dummy/users.json", JSON.stringify(users));

              response.status(200).send(
                JSON.stringify({
                  success: true,
                  message: "Password changed."
                })
              );
            } else {
              response.status(200).send(
                JSON.stringify({
                  success: false,
                  message: "Error changing password."
                })
              );
            }
          } else {
            response.status(200).send(
              JSON.stringify({
                success: false,
                message: "Link already expired."
              })
            );
          }
        } else {
          response.status(200).send(
            JSON.stringify({
              success: false,
              message: "Invalid link."
            })
          );
        }
      } else {
        response.status(200).send(
          JSON.stringify({
            success: false,
            message: "Invalid link."
          })
        );
      }
    })();
  });

  return api;
};
