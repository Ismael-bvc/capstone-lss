const bodyparser = require("body-parser");
const express = require("express");
const fs = require("fs");

module.exports = function() {
  var api = express.Router();

  api.use(bodyparser.urlencoded({ extended: false }));

  // Endpoint for service.
  api.get("/display", function(request, response) {
    (async function() {
      // Read from mock database.
      let list = [];

      try {
        list = JSON.parse(fs.readFileSync("./dummy/display.json", "utf-8"));
      } catch (error) {
        console.log(error);
      }

      //   console.log(list);

      // Look from code from services' table.
      const services = request.query.s;

      //   console.log(services);

      if (services) {
        const json = services
          .replace(/ /g, "")
          .split(",")
          .map(s => {
            return list.find(i => i.code.toLowerCase() === s.toLowerCase());
          });

        //console.log(json);

        response.status(200).send(JSON.stringify(json));
      } else {
        const json = {
          success: false,
          message: "Access Denied"
        };

        response.status(200).send(JSON.stringify(json));
      }
    })();
  });

  return api;
};
