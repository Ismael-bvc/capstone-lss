const bodyparser = require("body-parser");
const express = require("express");
const fs = require("fs");
const jwt = require("../modules/token.js");
const config = require("../config/config.json");

module.exports = function() {
    var api = express.Router();

    api.use(bodyparser.urlencoded({ extended: false }));

    // Endpoint for login.
    api.post("/review", function(request, response) {
        (async function() {
            //console.log(request);

            let reviews = [];
            console.log(request.body);

            try {
                reviews = JSON.parse(fs.readFileSync("./dummy/review.json", "utf-8"));
            } catch (e) {
                console.log(e);
            }

            const Desc = request.body.course;
            const Review = request.body.message;

            reviews.push({id: ""+reviews.length, course: Desc, review: Review});

            try{
                fs.writeFileSync("./dummy/review.json", JSON.stringify(reviews));
            }
            catch{
                let json = {
                    success: false,
                    message: "Adition FAILED."
                };
                response.status(200).send(JSON.stringify(json));
            }
        })();
    });

    return api;
};
