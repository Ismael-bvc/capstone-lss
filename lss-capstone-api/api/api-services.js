const bodyparser = require("body-parser");
const express = require("express");
const fs = require("fs");
const jwt = require("../modules/token.js");
const config = require("../config/config.json");

module.exports = function() {
    var api = express.Router();

    api.use(bodyparser.urlencoded({ extended: false }));

    /// Endpoint for adding a service.
    api.post("/service", function(request, response) {
        (async function() {
            // Read from mock database.
            let services = [];

            try {
                services = JSON.parse(
                    fs.readFileSync("./dummy/services.json", "utf-8")
                );
            } catch (error) {
                console.log(error);
            }

            // Look from info in the table.
            const sid = request.body.sid;
            const desc = request.body.desc;
            //const url = request.body.url;

            const newService = {
                sid: sid,
                desc: desc,
                colour: "lovely-blue"
            };

            // Add default fields.
            let defaults = [];

            try {
                defaults = JSON.parse(
                    fs.readFileSync("./dummy/defaults.json", "utf-8")
                );
            } catch (error) {
                console.log(error);
            }

            const fields = defaults.find(e => e.name === "fields");
            newService.fields = fields.fields;

            services.push(newService);
            fs.writeFileSync("./dummy/services.json", JSON.stringify(services));

            // This doesn't include jwt for now.
            const json = {
                success: true,
                message: "Service successfully added."
            };

            response.status(200).send(JSON.stringify(json));
        })();
    });

    /// Endpoint for getting service details.
    api.get("/service", function(request, response) {
        (async function() {
            // Read from mock database.
            let services = [];

            try {
                services = JSON.parse(
                    fs.readFileSync("./dummy/services.json", "utf-8")
                );
            } catch (error) {
                console.log(error);
            }

            // Look from code from services table.
            const sid = request.query.s;
            const token = request.query.t;

            // Validate token first!
            if (jwt.isValidToken(token, config.LoginSecret)) {
                if (sid) {
                    const service = services.find(
                        i => i.sid.toLowerCase() === sid.toLowerCase()
                    );

                    const fields = service.fields.map(e => {
                        return e;
                    });

                    const json = {
                        success: true,
                        message: "success",
                        result: {
                            sid: service.sid,
                            desc: service.desc,
                            colour: service.colour,
                            fields: fields
                        }
                    };

                    response.status(200).send(JSON.stringify(json));
                } else {
                    const json = {
                        success: false,
                        message: "Invalid parameters."
                    };

                    response.status(200).send(JSON.stringify(json));
                }
            } else {
                const json = {
                    success: false,
                    message: "Access Denied."
                };

                response.status(200).send(JSON.stringify(json));
            }
        })();
    });

    /// Endpoint for getting a list of services.
    api.get("/services", function(request, response) {
        (async function() {
            // Read from mock database.
            let services = [];

            try {
                services = JSON.parse(
                    fs.readFileSync("./dummy/services.json", "utf-8")
                );
            } catch (error) {
                console.log(error);
            }

            // Look from code from services table.
            const token = request.query.t;

            // Validate token first!
            //console.log(token);

            // For now, return all. But later, must return assigned services.
            const userServices = services.map(s => {
                // just return code and description.
                return { sid: s.sid, desc: s.desc };
            });

            const json = {
                success: true,
                message: "success",
                result: userServices
            };

            response.status(200).send(JSON.stringify(json));
        })();
    });

    return api;
};
