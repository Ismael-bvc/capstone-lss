const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const cors = require("cors");
const mongoose = require("mongoose");
const api = express.Router(); // does this need to be within the 'module.exports' function?
const Test = require("../models/test.model");
const config = require("../config/config.json");

module.exports = function() {
  api.use(bodyParser.urlencoded({ extended: true })); // change to 'false' for testing.
  app.use(cors()); // needed in order for nodemon to work.

  api.get("/test", async (request, response) => {
    const USERNAME = config.MongoDbAtlasUsername;
    const PASSWORD = config.MongoDbAtlasPassword;
    const DB_NAME = config.MongoDbAtlasDatabaseName;
    const URI =
      "mongodb+srv://" +
      USERNAME +
      ":" +
      PASSWORD +
      "@lss-cluster-zk9jl.mongodb.net/" +
      DB_NAME +
      "?retryWrites=true&w=majority";

    //
    // Making a connection to the MongoDB Atlas database.
    mongoose.connect(URI, { useNewUrlParser: true, useUnifiedTopology: true });

    const connection = mongoose.connection;

    connection.once("open", function() {
      console.log("MongoDB database connection established successfully.");
    });

    //
    // 'Test.find' returns a promise. The '.find()' method is a built-in method from Mongoose.
    // Mongoose has several built-in methods you can use if you choose to use Mongoose.
    const samples = await Test.find({});

    try {
      response.send(samples);
    } catch (e) {
      response.status(500).send(err);
    }
  });

  api.post("/test/add", async (request, response) => {
    const USERNAME = "lssDeveloper";
    const PASSWORD = "developer123";
    const DB_NAME = "test";
    const URI =
      "mongodb+srv://" +
      USERNAME +
      ":" +
      PASSWORD +
      "@lss-cluster-zk9jl.mongodb.net/" +
      DB_NAME +
      "?retryWrites=true&w=majority";

    //
    // Making a connection to the MongoDB Atlas database.
    mongoose.connect(URI, { useNewUrlParser: true, useUnifiedTopology: true });

    const connection = mongoose.connection;

    connection.once("open", function() {
      console.log("MongoDB database connection established successfully.");
    });

    const sample = new Test(request.body);

    try {
      await sample.save();
      response.send(sample);
    } catch (e) {
      response.status(500).send(e);
    }
  });

  return api;
};
