const bodyparser = require("body-parser");
const express = require("express");
const fs = require("fs");
const jwt = require("../modules/token.js");
const config = require("../config/config.json");

module.exports = function() {
    var api = express.Router();

    api.use(bodyparser.urlencoded({ extended: false }));

    // Endpoint for login.
    api.post("/booking", function(request, response) {
        (async function() {
            // Read from mock database.
            let bookings = [];

            try {
                bookings = JSON.parse(
                    fs.readFileSync("./dummy/booking.json", "utf-8")
                );
            } catch (error) {
                console.log(error);
            }

            // Look from username and password in users' table.
            const fname = request.body.firstname;
            const lname = request.body.lastname;
            const phone = request.body.phone;
            const email = request.body.email;
            const course = request.body.course;

            const book = {
                fname: fname,
                lname: lname,
                phone: phone,
                email: email,
                course: course
            };

            bookings.push(book);
            fs.writeFileSync("./dummy/booking.json", JSON.stringify(bookings));

            // This doesnt include jwt for now.
            const json = {
                success: true,
                message: "Booking successful"
            };

            response.status(200).send(JSON.stringify(json));
        })();
    });

    return api;
};
