const bodyparser = require("body-parser");
const express = require("express");
const fs = require("fs");
const jwt = require("../modules/token.js");
const config = require("../config/config.json");
module.exports = function() {
  var api = express.Router();
  api.use(bodyparser.urlencoded({ extended: false }));
  /// Endpoint for getting all users.
  api.get("/users", function(request, response) {
    //
    console.log("Hello, there.");
    (async function() {
      let users = [];
      try {
        users = JSON.parse(fs.readFileSync("./dummy/users.json", "utf-8"));
      } catch (error) {
        console.log(error);
      }
      // Look from the users table.
      const sid = request.query.s;
      const token = request.query.t;
      // Validate token.
      if (jwt.isValidToken(token, config.LoginSecret)) {
        if (sid) {
          const json = {
            success: true,
            message: "success",
            result: {
              sid: user.sid,
              name: user.name,
              roles: user.roles
            }
          };
          response.status(200).send(JSON.stringify(json));
        } else {
          const json = {
            success: false,
            message: "Invalid parameters."
          };
          response.status(200).send(JSON.stringify(json));
        }
      } else {
        const json = {
          success: false,
          message: "Access Denied."
        };
        response.status(200).send(JSON.stringify(json));
      }
    })();
  });
  // Endpoint for service.
  api.get("/user", function(request, response) {
    (async function() {
      // Read from mock database.
      let users = [];
      try {
        users = JSON.parse(fs.readFileSync("./dummy/users.json", "utf-8"));
      } catch (error) {
        console.log(error);
      }
      //console.log(users);
      // Look from code from services' table.
      const sid = request.query.sid;
      //console.log(sid);
      if (sid) {
        const user = users.find(i => i.sid.toLowerCase() === sid.toLowerCase());
        const json = {
          success: true,
          message: "success",
          result: user
        };
        response.status(200).send(JSON.stringify(json));
      } else {
        const json = {
          success: true,
          message: "success",
          result: users
        };
        response.status(200).send(JSON.stringify(json));
      }
    })();
  });
  // DON'T KNOW THE REASON -- PUT METHOD IS NOT WORKING BECAUSE OF SOME HEADER ISSUES - IF ANYONE HAS ANY IDEA WHY ITS HAPPENING PLEASE FIX IT
  api.post("/update-username", function(request, response) {
    (async function() {
      // Read from mock database.
      let users = [];
      try {
        users = JSON.parse(fs.readFileSync("./dummy/users.json", "utf-8"));
      } catch (error) {
        console.log(error);
      }
      const updatedUserName = request.body.updatedUserName;
      const currentUserName = request.body.currentUserName;
      if (users) {
        users.forEach(user => {
          if (user.name === currentUserName) user.name = updatedUserName;
        });
        fs.writeFileSync("./dummy/users.json", JSON.stringify(users));
        const json = {
          success: true,
          message: "user naame update successfull",
          result: { updatedUserName: updatedUserName }
        };
        response.status(200).send(JSON.stringify(json));
      } else {
        const json = {
          success: true,
          message: "user name update failed"
        };
        response.status(400).send(JSON.stringify(json));
      }
    })();
  });
  // Endpoint for update password .
  // DON'T KNOW THE REASON -- PUT METHOD IS NOT WORKING BECAUSE OF SOME HEADER ISSUES - IF ANYONE HAS ANY IDEA WHY ITS HAPPENING PLEASE FIX IT
  api.post("/update-password", function(request, response) {
    (async function() {
      // Validate first if user exists.
      let users = [];
      console.log("update-password");
      try {
        users = JSON.parse(fs.readFileSync("./dummy/users.json", "utf-8"));
        const token = request.body.token;
        // const currentPassword = request.body.currentPassword;
        const newPassword = request.body.newPassword;
        // const confirmPassWord = request.body.confirmPassWord;
        const currentUserName = request.body.currentUserName;
        console.log("##aw", newPassword, currentUserName);
        if (jwt.isValidToken(token, config.LoginSecret)) {
          if (users) {
            users.forEach(user => {
              if (user.name === currentUserName) {
                user.password = newPassword;
              }
              console.log("users", users);
              // Create new token.
              const payload = {
                sub: user.sub,
                iat: new Date().getTime()
              };
              const token = jwt.createToken(payload, config.LoginSecret);
              fs.writeFileSync("./dummy/users.json", JSON.stringify(users));
              response.status(200).send(
                JSON.stringify({
                  success: true,
                  message: "Password Update Successful",
                  token: token
                })
              );
            });
          } else {
            response.status(200).send(
              JSON.stringify({
                success: true,
                message: "Error changing password"
              })
            );
          }
        }
      } catch (e) {
        console.error(e);
      }
    })();
  });
  return api;
};
