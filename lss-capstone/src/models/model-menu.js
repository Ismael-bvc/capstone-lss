///
/// ModelMenu - Serves as a model for menu-drawer.
///
import { observable } from "mobx";

/// Model for logging-in.
export const ModelMenu = observable({
    open: true,
    menuCss: "drawer drawerOpen",
    pageCss: "content"
});

ModelMenu.getOpen = function() {
    return this.open;
}

ModelMenu.getMenuCss = function() {
    return this.menuCss;
}

ModelMenu.getPageCss = function() {
    return this.pageCss;
}

ModelMenu.toggle = function() {
    this.open = !this.open;
    this.menuCss = (this.open ? "drawer drawerOpen" : "drawer drawerClose");
    this.pageCss = (this.open ? "content" : "content content-min");
}