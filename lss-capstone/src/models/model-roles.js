import { observable } from "mobx";
import config from "../config.json";

export const ModelRole = observable({
  id: "",
  code: "", // SID / URL.
  desc: "",
  privs: [],
  servs: [],
  req: "",
  findRoleString: "",
  paging: {
    page: 1,
    row: 5
  },
  roles: []
});

/******************************
     Get and Set functions
 ******************************/
ModelRole.setId = function(value) {
  this.id = value;
};
ModelRole.getId = function() {
  return this.id;
};
ModelRole.setCode = function(value) {
  this.code = value;
};
ModelRole.getCode = function() {
  return this.code;
};
ModelRole.setDesc = function(value) {
  this.desc = value;
};
ModelRole.getDesc = function() {
  return this.desc;
};
ModelRole.setPrivs = function(value) {
  this.privs.push(value);
};
ModelRole.getPrivs = function() {
  return this.privs;
};
ModelRole.setServs = function(value) {
  this.servs.push(value);
};
ModelRole.getServs = function() {
  return this.servs;
};

// Clear the model.
ModelRole.clearAll = function() {
  this.id = "";
  this.code = "";
  this.desc = "";
  this.privs = [];
  this.servs = [];
};

ModelRole.setPageNumber = function(value) {
  this.paging.page = this.paging.page + value;
};
ModelRole.getPageNumber = function() {
  return this.paging.page;
};
ModelRole.setPageRow = function(value) {
  console.log(value);
  if (value === -1) this.paging.row = this.roles.length;
  else this.paging.row = value;
};
ModelRole.getPageRow = function() {
  return this.paging.row;
};
ModelRole.getRoles = function() {
  return this.roles;
};
ModelRole.getfindRole = function() {
  return this.findRoleString;
};
ModelRole.setfindRole = function(value) {
  this.findRoleString = value;
  let temRow = [];
  this.roles.forEach(element => {
    if (element.toLowerCase().includes(this.findRoleString.toLowerCase())) {
      temRow.push(element);
    }
  });
  this.roles = temRow;
};

/******************************
           Methods
 ******************************/

/// Connect to API to save data.
ModelRole.saveRoleData = async () => {
  const endpoint = config.api + "/role";
  const data = new URLSearchParams();
  data.append("code", ModelRole.code);
  data.append("desc", ModelRole.desc);
  data.append("privileges", ModelRole.privs);
  data.append("services", ModelRole.servs);

  console.log("Roles data:", data);

  let apiResponse = await fetch(endpoint, {
    method: "POST",
    body: data
  });
  let Reply = await apiResponse.json();

  console.log(Reply);
  if (Reply.success) {
    return Reply;
  }
};

ModelRole.getRoleData = async function() {
  const endpoint = config.api + "/role";

  let apiResponse = await fetch(endpoint, {
    method: "GET"
  });
  let Reply = await apiResponse.json();
  console.log(Reply);
  if (Reply.success) {
    console.log(this.findRoleString);
    if (this.findRoleString === "") {
      //this data is for testing the tabel The real return Replay will be used after demonstration
      this.roles = [
        "Role",
        "Role1",
        "Role2",
        "Role3",
        "Role4",
        "Role5",
        "Role6",
        "Role7",
        "Role8",
        "Role9",
        "Role10",
        "Role11",
        "Role12",
        "Role13",
        "Role14",
        "Role15",
        "Role16",
        "Role17",
        "Role18",
        "Role19",
        "Role20",
        "Role21",
        "Role22",
        "Role23",
        "Role24",
        "Role25",
        "Role26",
        "Role27",
        "Role27",
        "Role28",
        "Role29",
        "Role30"
      ];
    }
    return Reply;
  }
};
