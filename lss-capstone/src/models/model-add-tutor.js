import { observable, toJS } from "mobx";
import config from "../config.json";

const ModelAddTutor = observable({
  email: null,
  message: null
});

ModelAddTutor.setTutorEmail = function(email) {
  this.email = email;
};

ModelAddTutor.getTutorEmail = function() {
  return this.email;
};

ModelAddTutor.onSendInvite = async function() {
  const endpoint = config.api + "/addUser";

  try {
    var data = new URLSearchParams();
    data.append("email", this.email);

    console.log("##email", this.email);

    const reply = await fetch(endpoint, { method: "POST", body: data });
    console.log("#rep", reply);

    const result = await reply.json();

    console.log("#res", result);

    if (result) {
      if (result.success) {
        this.message = "";
        return true;
      } else {
        this.message = result.message;
      }
    }
  } catch (e) {
    console.log("model-add-tutor Exception! e --> ${e}");
  }

  this.message = "Error logging in.";
  return false;
};

export { ModelAddTutor };
