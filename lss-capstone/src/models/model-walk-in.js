///
/// ModelWalkIn - Serves as a model for service objects.
///

import { observable } from "mobx";
import config from "../config.json";
import ModelWalkInObject from "./model-service-object.js";
import modelGetCarousel from "./model-carousel.js";

/// Function for service page.
export const ModelWalkIn = observable({
  sid: "", // SID.
  desc: "", // Service name.
  colour: "",
  fields: []
});

ModelWalkIn.getCode = function() {
  return this.sid;
};

ModelWalkIn.setCode = function(value) {
  this.sid = value;
};

ModelWalkIn.getDesc = function() {
  return this.desc;
};

ModelWalkIn.setDesc = function(value) {
  this.desc = value;
};

ModelWalkIn.getColour = function(value) {
  console.log(this.colour);
  return this.colour;
};

ModelWalkIn.setColour = function(value) {
  this.colour = value;
};

ModelWalkIn.getFields = function() {
  return this.fields;
};
ModelWalkIn.setFields = function(value) {
  return this.value;
};
ModelWalkIn.reset = function() {
  this.sid = "";
  this.desc = "";
  this.colour = "";
  this.fields = [];
};

ModelWalkIn.getServiceData = async sid => {
  const endpoint = config.api + "/WalkInService?s=" + sid;
  let apiResponse = await fetch(endpoint, { method: "GET" });
  let Reply = await apiResponse.json();

  if (Reply.success) {
    ModelWalkIn.sid = Reply.result.sid;
    ModelWalkIn.desc = Reply.result.desc;
    ModelWalkIn.colour = Reply.result.colour;
    ModelWalkIn.fields = Reply.result.fields.map((f, index) => {
      const obj = Object.assign({}, ModelWalkInObject);
      obj.setOrder(index);
      obj.setCode(f.code);
      obj.setDesc(f.description);
      obj.setRequired(f.required);
      obj.setType(f.type);
      obj.setLength(f.length);
      obj.setDefault(f.default);
      obj.setEnabled(f.enabled);
      return obj;
    });

    console.log(ModelWalkIn.getCode());
  }
};
