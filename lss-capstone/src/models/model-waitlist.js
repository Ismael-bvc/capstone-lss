import { observable } from "mobx";

const ModelWaitlist = observable({});

ModelWaitlist.getWaitlist = async function() {
  let fetchData, reply, data;

  try {
    const endpoint =
      "https://bvc-vincibucket.s3.ca-central-1.amazonaws.com/waitlist-data.json";

    fetchData = {
      method: "GET",
      mode: "cors",
      cache: "no-cache",
      credentials: "omit",
      headers: {
        "Content-Type": "application/json",
        "x-amz-date": new Date().toUTCString(),
        "x-amz-acl": "public-read"
      }
    };

    reply = await fetch(endpoint, fetchData);
    data = await reply.json();

    return data;
  } catch (e) {
    console.log(`model-waitlist Exception! e --> ${e}`);
  }
};

export { ModelWaitlist };
