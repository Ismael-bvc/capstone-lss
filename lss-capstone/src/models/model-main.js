import { observable } from "mobx";
import { ModelWaitlist } from "./model-waitlist";
import { ModelLogin } from "./model-login.js";
import modelGetCarousel from "./model-carousel";
import { ModelUser } from "./model-user";
import { ModelTutee } from "./model-tutee";
import { ModelAppointment } from "./model-appointment";
import { ModelService } from "./model-service.js";
import { ModelRole } from "./model-roles.js";
import { ModelAddTutor } from "./model-add-tutor";
import { ModelReview } from "./model-review";
import { ModelProfile } from "./model-profile";
import { ModelToast } from "./model-toast.js";
import { ModelMenu } from "./model-menu.js";

/// Base model of the app. Holds values taken from the jwt token.
const ModelMain = observable({
  logged: false,
  jwt: "",
  iat: 0,
  sub: "",
  name: "",
  canDashboard: true,
  canList: false,
  canReports: false,
  canServices: false,
  canRoles: false,
  canUsers: false,
  canAdmin: false,
  services: [],
  loginModel: ModelLogin,
  userModel: ModelUser,
  roleModel: ModelRole,
  serviceModel: ModelService,
  displayModel: modelGetCarousel,
  // tutorModel: ModelTutor,
  tuteeModel: ModelTutee,
  appointmentModel: ModelAppointment,
  reviewModel: ModelReview,
  addTutor: ModelAddTutor,
  toast: ModelToast,
  profile: ModelProfile,
  menuModel: ModelMenu
});

/*****************************
  Get and Set for properties
******************************/
ModelMain.isLogged = function() {
  return this.logged;
};

ModelMain.getSub = function() {
  return this.sub;
};

ModelMain.setSub = function(value) {
  this.sub = value;
};

ModelMain.getService = function() {
  return this.service;
};

ModelMain.getServices = function() {
  return this.services;
};

ModelMain.setService = function(value) {
  this.service = value;
};

ModelMain.getCanDashboard = function() {
  return this.canDashboard;
};

ModelMain.getCanLists = function() {
  return this.canList;
};

ModelMain.getCanReports = function() {
  return this.canReports;
};

ModelMain.getCanRoles = function() {
  return this.canRoles;
};

ModelMain.getCanServices = function() {
  return this.canServices;
};

ModelMain.getCanUsers = function() {
  return this.canUsers;
};

ModelMain.getCanAdmin = function() {
  return this.canAdmin;
};

ModelMain.getToken = function() {
  return this.jwt;
};

/**********
  Methods
***********/

//
ModelMain.setCredentials = function(value) {
  if (value) {
    const parts = value.split(".");

    // If token not equal to 3 parts, return.
    if (parts.length != 3) {
      return false;
    }

    try {
      const json = JSON.parse(window.atob(parts[1]));
      this.jwt = value;
      this.sub = json.sub;
      this.iat = json.iat;
      this.name = json.nam;
      this.canAdmin = json.prv.includes("adm");
      this.canDashboard = json.prv.includes("brd") || this.canAdmin;
      this.canList = json.prv.includes("lst") || this.canAdmin;
      this.canReports = json.prv.includes("rpt") || this.canAdmin;
      this.canRoles = json.prv.includes("rol") || this.canAdmin;
      this.canServices = json.prv.includes("svc") || this.canAdmin;
      this.canUsers = json.prv.includes("usr") || this.canAdmin;
      this.services = json.svc.map(s => {
        return s;
      });

      // If everything went well...
      this.logged = true;

      return true;
    } catch (ex) {
      return false;
    }
  }

  return false;
};

ModelMain.reset = function() {
  // Reset each values.
  this.logged = false;
  this.jwt = "";
  this.sub = "";
  this.iat = 0;
  this.name = "";
  this.canDashboard = true;
  this.canList = false;
  this.canReports = false;
  this.canServices = false;
  this.canRoles = false;
  this.canUsers = false;
  this.canAdmin = false;
  this.services = [];

  // Reset each models.
  this.loginModel.reset();
  /*
  userModel: null,
  roleModel: null,
  serviceModel: null,
  displayModel: modelGetCarousel,
  appointmentModel: ModelAppointment
  */
};

ModelMain.logout = function() {
  ModelMain.reset();
};

/// Generate SID based on the role name.
/// Currently DOES NOT verify the SID does not exist.
/// Params:
///     @name - the name of whatever you want to generate an SID for. (Ex:
///             "Dylan's Tutoring Services").
ModelMain.generateSid = function(name) {
  let sidPrefix = null;

  // If this.desc is multiple words, create initialism,
  // and append the last word to the end.
  if (name.includes(" ")) {
    // '/g' = 'global'; Continues searching until end of string.
    sidPrefix = name.match(/\b(\w)/g);
    sidPrefix = sidPrefix.join("").toUpperCase();

    let lastWord = name.split(" ");
    lastWord = lastWord[lastWord.length - 1];

    // Replace the last char with the entire last word.
    sidPrefix = sidPrefix.substring(0, sidPrefix.length - 1);
    sidPrefix += lastWord;
  } else {
    // Else, save the single word.
    sidPrefix = name;
  }

  // Creates an SID beginning with "sidPrefix" followed by an undrescore, then
  // a pseudo-random eight digit ID.
  let sid = require("node-sid")().create(sidPrefix, 6);
  // Remove the underscore.
  sid = sid.replace("_", "");

  return sid;
};

export default ModelMain;
