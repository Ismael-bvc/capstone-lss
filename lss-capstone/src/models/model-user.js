import { observable, toJS } from "mobx";
import config from "../config.json";
import moment from "moment";

// Remove this import after creating API for fetching user data.
import users from "../appointments-data";

const ModelUser = observable({
  email: "",
  password: "",
  tutorId: null,
  tutorName: null,
  currentWeekSchedule: null,
  nextWeekSchedule: null,
  selectedDaySchedule: null,
  currentWeekAppointments: [],
  nextWeekAppointments: []
});

/*****************
 *  Get and Set  *
 *****************/
ModelUser.getTutorId = function() {
  return this.tutorId;
};
ModelUser.getTutorName = function() {
  return this.tutorName;
};
ModelUser.getCurrentWeekSchedule = function() {
  return this.currentWeekSchedule;
};
ModelUser.nextWeekSchedule = function() {
  return this.nextWeekSchedule;
};
ModelUser.getCurrentWeekAppointments = function() {
  return this.currentWeekAppointments;
};
ModelUser.getNextWeekAppointments = function() {
  return this.nextWeekAppointments;
};

/******************
 *    Methods     *
 ******************/
/// Retrieves all users from the database.
// ModelUser.getUsers = async () => {
//   console.log("Is this thing on?");
//   const endpoint = config.api + "/users" + localStorage.getItem(config.authkey);

//   let apiResponse = await fetch(endpoint, { method: "GET" });
//   let Reply = await apiResponse.json();

//   if (Reply.success) {
//     console.log(Reply);
//     return Reply;
//   }
// };
ModelUser.getUsers = async () => {
  let fuckoff = "Get fucked.";
  console.log(fuckoff);
  return fuckoff;
};

ModelUser.setAppointment = function(appointment) {
  let hasAlreadySetAppointment;

  //
  // Check is the appointment is for the current or next week.
  if (appointment.isCurrentWeek) {
    //
    // Checks if the user has already set an appointment on the same day.
    hasAlreadySetAppointment = this.currentWeekAppointments.find(
      currentAppointment => {
        return (
          appointment.email === currentAppointment.email &&
          appointment.month === currentAppointment.month &&
          appointment.date === currentAppointment.date
        );
      }
    );

    if (hasAlreadySetAppointment) {
      console.log("Cannot have more than 1 appointment per day.");
    } else {
      console.log("Appointment successfully ");
      this.currentWeekAppointments.push(appointment);
    }
  } else {
    hasAlreadySetAppointment = this.nextWeekAppointments.find(
      currentAppointment => {
        return (
          appointment.email === currentAppointment.email &&
          appointment.month === currentAppointment.month &&
          appointment.date === currentAppointment.date
        );
      }
    );

    if (hasAlreadySetAppointment) {
      console.log("Cannot have more than 1 appointment per day.");
    } else {
      console.log("Appointment successfully ");
      this.nextWeekAppointments.push(appointment);
    }
  }
  console.log(toJS(this.currentWeekAppointments));
};

ModelUser.fetchTutorData = async function(id) {
  try {
    if (id) {
      const tutors = JSON.parse(users);

      tutors.forEach(user => {
        if (user._id === id) {
          this.tutorId = user._id;
          this.tutorName = user.name;
          this.currentWeekSchedule = user.schedule;
          this.nextWeekSchedule = user.schedule;
        }
      });
    }
  } catch (e) {
    console.log(`FROM fetchTutorData IN model-appointment --> ${e} `);
  }
};

//
// Returns every day of the current week.
ModelUser.getCurrentWeek = function() {
  let currentDate = moment();

  let weekStart = currentDate.clone().startOf("isoWeek");

  const daysOfTheWeek = 7;

  let days = [];

  for (let i = 0; i <= daysOfTheWeek - 1; i++) {
    days.push({
      day: moment(weekStart)
        .add(i, "days")
        .format("dddd"),
      date: moment(weekStart)
        .add(i, "days")
        .format("DD"),
      month: moment(weekStart)
        .add(i, "days")
        .format("MMMM")
    });
  }

  // this.tutorAvailableDays = days;

  return days;
};

//
// Returns every day of the following week.
ModelUser.getNextWeek = function() {
  let currentDate = moment();

  let weekNext = currentDate
    .clone()
    .startOf("isoWeek")
    .add(7, "days");
  const daysOfTheWeek = 7;

  let days = [];

  for (let i = 0; i <= daysOfTheWeek - 1; i++) {
    days.push({
      day: moment(weekNext)
        .add(i, "days")
        .format("dddd"),
      date: moment(weekNext)
        .add(i, "days")
        .format("DD"),
      month: moment(weekNext)
        .add(i, "days")
        .format("MMMM")
    });
  }

  // this.tutorAvailableDays = days;

  return days;
};

//
// Update this method to an async method after creating an API to
// fetch tutor data.
ModelUser.getAvailableDayTimes = function(value) {
  //
  // Searches through the currentWeekSchedule array. Set the 'selectedDaySchedule' property
  // to an object whose 'day' property is equal to whatever was passed to this function's
  // 'value' argument. For example, if 'Monday' was passed as an argument then 'currentWeekSchedule'
  // will have the time schedule for Monday.
  // You must first use the fetchTutorData function for currentWeekSchedule to not be null.
  if (this.currentWeekSchedule) {
    this.currentWeekSchedule.forEach(schedule => {
      if (schedule.day === value) {
        //
        // ATTENTION! This line should be deleted after creating the backend and the scheduler customier
        // component. The reason is once the tutor sets their schedule, the schedule customizer component
        // will use Moment and its .format method to display the correct output.
        this.selectedDaySchedule = schedule.times.map(time =>
          //
          // If you don't format each string of the 'selectedDaySchedule' array, each string
          // will remain looking something like this: 2020-02-21T16:00:00.0000Z
          moment(time).format("LT")
        );

        // console.log(`Day value is a weekday -> ${value}`);
      } else if (value === "Saturday" || value === "Sunday") {
        this.selectedDaySchedule = null;
        // console.log(`Day value is a weekend -> ${value}`);
      }
    });
  }
};

ModelUser.firstDayOfMonth = function() {
  let dateObject = this.dateObject;
  let firstDay = moment(dateObject)
    .startOf("month")
    .format("d");

  return firstDay;
};

ModelUser.getUsers = function() {};

export { ModelUser };
