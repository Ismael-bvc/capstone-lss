///
/// ModelService - Serves as a model for service objects.
///

import { observable } from "mobx";
import config from "../config.json";
import ModelServiceObject from "./model-service-object.js";
import modelGetCarousel from "./model-carousel.js";

/// Function for service page.
export const ModelService = observable({
  id: "",
  sid: "", // SID.
  desc: "", // Service name.
  colour: "",
  req: "",
  findServiceString: "",
  paging: {
    page: 1,
    row: 5
  },
  fields: [],
  serviceslist: [],
  users: [], // Temp workaround
  roles: [] // Temp workaround
});

///
/// Get and Set functions.
///
ModelService.getId = function() {
  return this.id;
};

ModelService.setId = function(value) {
  this.id = value;
};

ModelService.getCode = function() {
  return this.sid;
};

ModelService.setCode = function(value) {
  this.sid = value;
};

ModelService.getDesc = function() {
  return this.desc;
};

ModelService.setDesc = function(value) {
  this.desc = value;
};

ModelService.getUrl = function() {
  return this.url;
};

ModelService.setUrl = function(value) {
  this.url = value;
};

ModelService.getColour = function(value) {
  console.log(this.colour);
  return this.colour;
};

ModelService.setColour = function(value) {
  this.colour = value;
};

ModelService.getServicesList = function() {
  return this.serviceslist;
};

ModelService.getFields = function() {
  return this.fields;
};

ModelService.setPageNumber = function(value) {
  this.paging.page = this.paging.page + value;
};
ModelService.getPageNumber = function() {
  return this.paging.page;
};
ModelService.setPageRow = function(value) {
  console.log(value);
  if (value === -1) this.paging.row = this.serviceslist.length;
  else this.paging.row = value;
};
ModelService.getPageRow = function() {
  return this.paging.row;
};

ModelService.setFindService = function(value) {
  this.findServiceString = value;
  let temRow = [];
  this.serviceslist.forEach(element => {
    if (element.toLowerCase().includes(this.findServiceString.toLowerCase())) {
      temRow.push(element);
    }
  });
  this.serviceslist = temRow;
};

ModelService.setPageNumber = function(value) {
  this.paging.page = this.paging.page + value;
};
ModelService.getPageNumber = function() {
  return this.paging.page;
};
ModelService.setPageRow = function(value) {
  console.log(value);
  if (value === -1) this.paging.row = this.serviceslist.length;
  else this.paging.row = value;
};
ModelService.getPageRow = function() {
  return this.paging.row;
};

ModelService.setUsers = async function() {
  let usersList = await this.props.model.roleModel.getRoleData();
  this.users = usersList.Array;
};

ModelService.getUsers = function() {
  return this.users;
};

///
/// Methods
///

/// Connects to the database to get a specific service.
ModelService.getServiceData = async sid => {
  const endpoint =
    config.api +
    "/service?s=" +
    sid +
    "&t=" +
    localStorage.getItem(config.authkey);

  let apiResponse = await fetch(endpoint, { method: "GET" });
  let Reply = await apiResponse.json();

  if (Reply.success) {
    ModelService.sid = Reply.result.sid;
    ModelService.desc = Reply.result.desc;
    ModelService.fields = Reply.result.fields.map((f, index) => {
      const obj = Object.assign({}, ModelServiceObject);
      obj.setOrder(index);
      obj.setCode(f.code);
      obj.setDesc(f.description);
      obj.setRequired(f.required);
      obj.setType(f.type);
      obj.setLength(f.length);
      obj.setDefault(f.default);
      obj.setEnabled(f.enabled);
      return obj;
    });
  }
};

/// Connects to the database to get all services.
ModelService.getServicesData = async () => {
  const endpoint =
    config.api + "/services?t=" + localStorage.getItem(config.authkey);

  let apiResponse = await fetch(endpoint, { method: "GET" });
  let Reply = await apiResponse.json();

  if (Reply.success) {
    ModelService.serviceslist = Reply.result.map((s, index) => {
      const obj = Object.assign({}, ModelServiceObject);
      obj.setOrder(index);
      obj.setCode(s.sid);
      obj.setDesc(s.desc);
      return obj;
    });
  }
};

/// Saves the data to the database.
ModelService.saveServiceData = async () => {
  const endpoint = config.api + "/service";

  const data = new URLSearchParams();
  data.append("sid", ModelService.sid);
  data.append("desc", ModelService.desc);
  data.append("url", ModelService.url);
  data.append("colour", ModelService.colour);

  let apiResponse = await fetch(endpoint, {
    method: "POST",
    body: data
  });
  let Reply = await apiResponse.json();

  console.log(Reply);
  if (Reply.success) {
    if (this.findServiceString === "") {
      //this data is for testing the tabel The real return Replay will be used after demonstration
      this.serviceslist = [
        "Role",
        "Role1",
        "Role2",
        "Role3",
        "Role4",
        "Role5",
        "Role6",
        "Role7",
        "Role8",
        "Role9",
        "Role10",
        "Role11",
        "Role12",
        "Role13",
        "Role14",
        "Role15",
        "Role16",
        "Role17",
        "Role18",
        "Role19",
        "Role20",
        "Role21",
        "Role22",
        "Role23",
        "Role24",
        "Role25",
        "Role26",
        "Role27",
        "Role27",
        "Role28",
        "Role29",
        "Role30"
      ];
    }
    return Reply;
  }
};

export default ModelService;
