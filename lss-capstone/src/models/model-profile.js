import { observable } from "mobx";
import config from "../config.json";
import { ModelLogin } from "./model-login.js";
import axios from "axios";
const qs = require("querystring");

const ModelProfile = observable({
  currentUserName: "",
  updatedUserName: "",
  currentPassword: "",
  newPassword: "",
  confirmPassWord: "",
  passwordErrorMessage: "",
  userErrorMessage: ""
});

ModelProfile.setUpdatedUserName = function(value) {
  this.updatedUserName = value;
};

ModelProfile.setCurrentUserName = function(value) {
  this.currentUserName = value;
};

ModelProfile.getCurrentUserName = function() {
  return this.currentUserName;
};

ModelProfile.getNewPassword = function() {
  return this.newPassword;
};

ModelProfile.getConfirmPassword = function() {
  return this.confirmPassWord;
};

ModelProfile.getUpdatedUserName = function() {
  return this.updatedUserName;
};

ModelProfile.setCurrentPassword = function(value) {
  this.currentPassword = value;
};

ModelProfile.setNewPassword = function(value) {
  this.newPassword = value;
};

ModelProfile.setConfirmPassword = function(value) {
  this.confirmPassWord = value;
};

ModelProfile.getPasswordErrorMessage = function() {
  return this.passwordErrorMessage;
};

ModelProfile.getUserErrorMessage = function() {
  return this.userErrorMessage;
};

ModelProfile.fetchUserDetails = async function() {
  const endpoint = config.api + "/user";

  const userEmail = ModelLogin.getUsername();

  try {
    const reply = await fetch(endpoint, { method: "GET" });

    const response = await reply.json();

    if (response) {
      if (response.success) {
        response.result.forEach(i => {
          if (i.sub === userEmail) {
            this.setCurrentUserName(i.name);
          }
        });
      } else {
        this.setCurrentUserName("");
      }
    }
  } catch (e) {
    console.log("model-prfile Exception! e --> ${e}");
  }
};

ModelProfile.updateUserName = async function() {
  const endpoint = config.api + "/update-username";
  if (!this.updatedUserName) {
    this.userErrorMessage = "Please enter your username.";
    return false;
  }

  let data = new URLSearchParams();
  data.append("updatedUserName", this.updatedUserName);
  data.append("currentUserName", this.currentUserName);

  const putMethod = {
    method: "POST", // Method itself
    headers: {
      "Content-type": "application/x-www-form-urlencoded" // Indicates the content
    },
    body: data // We send data in JSON format
  };

  fetch(endpoint, putMethod)
    .then(response => response.json())
    .then(data => {
      this.userErrorMessage = data.message;
    })
    .catch(e => console.log(`model-profile Exception! e --> ${e}`));

  this.userErrorMessage = "Error changing username.";
  return false;
};

ModelProfile.updatePassword = async function() {
  if (!this.newPassword) {
    this.passwordErrorMessage = "Please enter new password.";
    return false;
  }

  if (!this.confirmPassWord) {
    this.passwordErrorMessage = "Please confirm new password.";
    return false;
  }

  if (this.confirmPassWord !== this.newPassword) {
    this.passwordErrorMessage = "Passwords do not match.";
    return false;
  }

  const endpoint = config.api + "/update-password";
  try {
    let token = localStorage.getItem(config.authkey);
    alert(token);

    localStorage.removeItem(config.authkey);

    var data = new URLSearchParams();
    data.append("token", token);
    data.append("currentPassword", this.currentPassword);
    data.append("newPassword", this.newPassword);
    data.append("confirmPassWord", this.confirmPassWord);
    data.append("currentUserName", this.currentUserName);

    const reply = await fetch(endpoint, { method: "POST", body: data });
    const result = await reply.json();

    if (result) {
      if (result.success) {
        this.passwordErrorMessage = result.message;
        localStorage.setItem(config.authkey, result.token);

        return true;
      } else {
        this.passwordErrorMessage = result.message;
        return false;
      }
    }
  } catch (e) {
    console.log("model-profile Exception! e --> ${e}");
  }

  this.message = "Error changing password.";
  return false;
};

export { ModelProfile };
