import { observable } from "mobx";
import React from "react";
import { Link } from "react-router-dom";

export const ModelOfferred = observable({
  findOfferString: "",
  paging: {
    page: 1,
    row: 5
  },
  offers: [
    <Link to="/WalkInBook/CTTutor">
      Creative Technologies Tutorials (Walk-In)
    </Link>,
    <Link to="/appointment">English Language Learning (Appointment)</Link>
  ]
});

/******************************
     Get and Set functions
 ******************************/

ModelOfferred.setPageNumber = function(value) {
  this.paging.page = this.paging.page + value;
};
ModelOfferred.getPageNumber = function() {
  return this.paging.page;
};
ModelOfferred.setPageRow = function(value) {
  console.log(value);
  if (value === -1) this.paging.row = this.offers.length;
  else this.paging.row = value;
};
ModelOfferred.getPageRow = function() {
  return this.paging.row;
};
ModelOfferred.getOffers = function() {
  return this.offers;
};
