import { observable, toJS } from "mobx";
import moment from "moment";

const ModelAppointment = observable({
  month: null,
  date: null,
  day: null,
  startTime: null,
  isCurrentWeek: null,
  tuteeName: null,
  tuteeEmail: null,
  tuteePhone: null,
  tuteeSpecialRequest: null
});

ModelAppointment.getAppointment = function() {
  return {
    month: this.month,
    date: this.date,
    day: this.day,
    startTime: this.startTime,
    isCurrentWeek: this.isCurrentWeek,
    tuteeName: this.tuteeName,
    tuteeEmail: this.tuteeEmail,
    tuteePhone: this.tuteePhone,
    tuteeSpecialRequest: this.tuteeSpecialRequest
  };
};

ModelAppointment.getIsCurrentWeek = function() {
  return this.isCurrentWeek;
};

ModelAppointment.getStartTime = function() {
  return this.startTime;
};

ModelAppointment.getDay = function() {
  return this.day;
};

ModelAppointment.getDate = function() {
  return this.date;
};

ModelAppointment.getMonth = function() {
  return this.month;
};

ModelAppointment.getTuteeName = function() {
  return this.tuteeName;
};
ModelAppointment.getTuteeEmail = function() {
  return this.tuteeEmail;
};
ModelAppointment.getTuteePhone = function() {
  return this.tuteePhone;
};

ModelAppointment.getSpecialRequest = function() {
  return this.specialRequest;
};

ModelAppointment.setSpecialRequest = function(specialRequest) {
  this.tuteeSpecialRequest = specialRequest;
};

ModelAppointment.setMonth = function(month) {
  this.month = month;
};

ModelAppointment.setDate = function(date) {
  this.date = date;
};

ModelAppointment.setDay = function(day) {
  this.day = day;
};

ModelAppointment.setStartTime = function(startTime) {
  this.startTime = startTime;
};

ModelAppointment.setTuteeName = function(name) {
  this.tuteeName = name;
};
ModelAppointment.setTuteeEmail = function(email) {
  this.tuteeEmail = email;
};
ModelAppointment.setTuteePhone = function(phone) {
  this.tuteePhone = phone;
};

ModelAppointment.setIsCurrentWeek = function(isCurrentWeek) {
  this.isCurrentWeek = isCurrentWeek;
};

export { ModelAppointment };
