import { observable } from "mobx";

/// Function for service page.
export const ModelServiceObject = observable({
  order: 0,  
  code: "",
  desc: "",
  required: false,
  type: "",
  length: 0,
  default: false,
  enabled: false
});

ModelServiceObject.getOrder = function() {
    return this.order;
}

ModelServiceObject.setOrder = function(value) {
    this.order = value;
}

ModelServiceObject.getCode = function() {
    return this.code;
}

ModelServiceObject.setCode = function(value) {
    this.code = value;
}

ModelServiceObject.getDesc = function() {
    return this.desc;
}

ModelServiceObject.setDesc = function(value) {
    this.desc = value;
}

ModelServiceObject.getRequired = function() {
    return this.required;
}

ModelServiceObject.setRequired = function(value) {
    this.required = value;
}

ModelServiceObject.getType = function() {
    return this.type;
}

ModelServiceObject.setType = function(value) {
    this.type = value;
}

ModelServiceObject.getLength = function() {
    return this.length;
}

ModelServiceObject.setLength = function(value) {
    this.length = value;
}

ModelServiceObject.getDefault = function() {
    return this.default;
}

ModelServiceObject.setDefault = function(value) {
    this.default = value;
}

ModelServiceObject.getEnabled = function() {
    return this.enabled;
}

ModelServiceObject.setEnabled = function(value) {
    this.enabled = value;
}

ModelServiceObject.reset = function() {
    this.order = 0;
    this.code = "";
    this.desc = "";
    this.required = false;
    this.type = "";
    this.length = 0;
    this.default = false;
    this.enabled = false;
}

export default ModelServiceObject;