import { observable } from "mobx";
import config  from "../config.json";

const modelGetCarousel = observable({
  index:0,
  display:"",
  oneFetch:[""],
  courses:[{name:"Creative Technology Tutorials",code:"CTTutor",state:true},
  {name:"Practical Nursing",code:"PNTutor",state:true},
  {name:"English Language Learning",code:"ELL",state:true},
  {name:"Reboot",code:"Reboot",state:true}],

  waitlists:"",
  courslist:""
});

/// This function fetches the list of Tutees and the course they are wating for
/// Params: 
///    @data:the result given by the Api call
modelGetCarousel.GetCarousel = async function() {

  //console.log("s= "+this.oneFetch[this.index]+" index: "+this.index);
  const endpoint = config.api + "/display?s="+this.oneFetch[this.index];

  try {
      const reply = await fetch(endpoint, { method: "GET" }); 
      const result = await reply.json();
      let data = result;
        //let tempObj ={status:element.status, name:element.desc, queue:element.schedule};
        this.waitlists = JSON.stringify(data[0].list);
        this.courslist = data[0].desc;

        // await console.log(this.waitlists[0]);
        // await console.log(data[0].list);
  }
  catch (e) {
      console.log(`model-waitlist Exception! e --> ${e}`);
  }

}

/// This function sets the courses to be waitlistsed on the carousel
/// Params: 
///    @NewValue:the user input for courslistes to be waitlistsed
modelGetCarousel.SetWaitLists = function(NewValue)
{
  this.waitlists = NewValue;
}

/// This function retrives each waiting list individualy
/// Params: 
modelGetCarousel.GetWaitListData = function()
{
  //console.log(this.waitlists);
  return (this.waitlists);
}

/// This function retrives each course name individualy
/// Params: 
modelGetCarousel.GetdescData = function()
{
  //console.log(this.courslist);
  return (this.courslist);
}

/// This funcion syncronizes and iterate through index
/// Params: 
modelGetCarousel.SetIndex = function()
{
  console.log(this.index);
    if(this.index < this.oneFetch.length -1)
      this.index++;
    else
      this.index = 0;
}

/// This function retrives each course name individualy
/// Params: 
modelGetCarousel.GetNumberOfCourses = function()
{
  return (this.courses.length);
}

modelGetCarousel.GetCoursesforCheckbox = function()
{
  return (this.courses);
}

modelGetCarousel.CheckboxStateChanger = function(value)
{
  for(let i = 0; i<this.courses.length; i++)
  {
    if(this.courses[i].name === value)
    {
      //console.log(this.courses[i].name+" == "+value);
      this.courses[i].state = !this.courses[i].state;
      break;
    }
  }
}

modelGetCarousel.ChangeDisplay = function()
{
  let j =0;
  for(let i = 0; i<this.courses.length; i++)
  {
    if(this.courses[i].state === true)
    {
      this.oneFetch[j]=this.courses[i].code;
      j++;
    }
  }

}


export default modelGetCarousel;






