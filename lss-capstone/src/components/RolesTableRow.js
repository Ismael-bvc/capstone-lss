import React, { Component } from "react";
import { observer } from "mobx-react";
import TableCell from "@material-ui/core/TableCell";

import TableRow from "@material-ui/core/TableRow";

/** PROPS
 * @list: array of objects
 */
export default observer(
class DisplayRoleRow extends Component {

  tableRows = () => {
        //let findRole = this.props.model.roleModel.getfindRole();
        let page= this.props.props.model.roleModel.getPageNumber();
        let rowperpage = this.props.props.model.roleModel.getPageRow();
        let roles = this.props.props.model.roleModel.getRoles();
        let row = [];
        if(roles.length>0)
        {
            for(let i = (page-1)*rowperpage; i< ((page-1)*rowperpage)+rowperpage; i++)
            {
                if(roles[i]===undefined)continue;
              row.push(<TableRow><TableCell>{roles[i]}</TableCell></TableRow>);
            }
        }
        else row.push(<TableRow><TableCell>No Result found</TableCell></TableRow>);

      return row;
  };

  render() {

    return (<>{this.tableRows()}</>);
  }
});
