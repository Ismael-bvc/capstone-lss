import React, { Component } from "react";
import ".././styles/form-colourSelection.css";

///
/// Props:
///   onSelect: expects callback function that component uses to signal parent.
///
export class ColourSelection extends Component {
  ///
  /// Sends callback to parent with the colour value selected by the user
  /// and displays the selected colour at the bottom of the component.
  /// Params:
  ///   @colour - The colour selected by the user.
  ///
  selectColour = colour => {
    // Display the selected colour.
    let selectionView = document.getElementsByClassName("selection");
    selectionView[0].setAttribute("id", colour);

    // Get the callback function from props.
    const Callback = this.props.onSelect;
    // Ensure it is not null.
    if (Callback) Callback(selectionView[0].id);
  };

  ///
  render() {
    return (
      <div className="colour-component-container">
        <h4>Select theme colour</h4>
        <div className="colour-container">
          <div
            id="cool-pink"
            className="colour-button"
            onClick={e => this.selectColour(e.target.id)}
          />
          <div
            id="bubblegum"
            className="colour-button"
            onClick={e => this.selectColour(e.target.id)}
          />
          <div
            id="fresh-lavender"
            className="colour-button"
            onClick={e => this.selectColour(e.target.id)}
          />
          <div
            id="golly-purple"
            className="colour-button"
            onClick={e => this.selectColour(e.target.id)}
          />
          <div
            id="deep-purple"
            className="colour-button"
            onClick={e => this.selectColour(e.target.id)}
          />

          <div
            id="navy"
            className="colour-button"
            onClick={e => this.selectColour(e.target.id)}
          />
          <div
            id="lovely-blue"
            className="colour-button"
            onClick={e => this.selectColour(e.target.id)}
          />
          <div
            id="sky"
            className="colour-button"
            onClick={e => this.selectColour(e.target.id)}
          />
          <div
            id="oceania"
            className="colour-button"
            onClick={e => this.selectColour(e.target.id)}
          />
          <div
            id="lush"
            className="colour-button"
            onClick={e => this.selectColour(e.target.id)}
          />

          <div
            id="sunflower"
            className="colour-button"
            onClick={e => this.selectColour(e.target.id)}
          />
          <div
            id="lorange"
            className="colour-button"
            onClick={e => this.selectColour(e.target.id)}
          />
          <div
            id="pumpkin-pie"
            className="colour-button"
            onClick={e => this.selectColour(e.target.id)}
          />
          <div
            id="brighter-red"
            className="colour-button"
            onClick={e => this.selectColour(e.target.id)}
          />
          <div
            id="almost-maroon"
            className="colour-button"
            onClick={e => this.selectColour(e.target.id)}
          />
        </div>

        <div className="display-selection">
          <div className="selection"></div>
        </div>
      </div>
    );
  }
}
