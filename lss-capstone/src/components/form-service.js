import React, { Component } from "react";
import ".././styles/form-addService.css";

import { ColourSelection } from "./form-colourSelection.js";

import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import SaveIcon from "@material-ui/icons/Save";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import OutlinedInput from "@material-ui/core/OutlinedInput";
import InputAdornment from "@material-ui/core/InputAdornment";
import Tooltip from "@material-ui/core/Tooltip";
import Checkbox from "@material-ui/core/Checkbox";
import FormGroup from "@material-ui/core/FormGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormLabel from "@material-ui/core/FormLabel";
import { toast } from "react-toastify";

// TODO: isUrlAvailable

///
/// Props:
///   @model: MainModel which holds every other model.
///   @serviceName: Optional. Name of service to appear in "Service name" input box.
///   @url: Optional. The custom URL of a service to be modified. Appears in "URL" input box.
export class Service extends Component {
  /// Makes a callback to model-service.js
  saveService = async () => {
    let serviceName = this.serviceNameInput.value;
    let url = this.urlInput.value;
    let sid = "";

    // If inputs are valid, save the information.
    if (this.isServiceNameValid() && this.isUrlValid()) {
      sid = this.props.model.generateSid(serviceName);

      this.props.model.serviceModel.setCode(url);
      this.props.model.serviceModel.setDesc(serviceName);

      const reply = await this.props.model.serviceModel.saveServiceData(
        sid,
        serviceName,
        url
      );

      if (reply.success) {
        toast.success("Service successfully saved.", {
          className: "toast-success toast-base"
        });
      } else {
        toast.error("An error ocurred. Service could not be saved.", {
          className: "toast-failure toast-base"
        });
        return false;
      }
    } else {
      toast.warn("Please ensure all fields are entered correctly.", {
        className: "toast-warning toast-base"
      });
    }
  };

  /// Checks the input for the service's name for acceptable inputs values.
  isServiceNameValid = () => {
    let serviceName = this.serviceNameInput.value;
    serviceName = serviceName.trim();
    let regex = new RegExp(/[^0-9a-zA-Z'_-\s]+$/);

    if (regex.test(serviceName) || serviceName === "") {
      return false;
    } else {
      console.log("servicename is valid.");
      return true;
    }
  };

  /// Checks the user's custom URL for acceptable inputs.
  isUrlValid = () => {
    let url = this.urlInput.value;
    url = url.trim();
    let regex = new RegExp("[^0-9a-zA-Z'_-]+$");

    // Check that URL is valid.
    if (regex.test(url) || url === "") {
      return false;
    } else if (true) {
      // Ensure URL is not already used.
      console.log("url fine.");
      return true;
    } else {
      return false;
    }
  };

  /// Save the colour in ModelService.
  /// Props:
  ///   @colour: Colour that will be saved.
  saveColour = colour => {
    this.props.model.serviceModel.setColour(colour);
  };

  // The following is a separate user story:
  // /// Retrieves all users.
  // getUsers = async () => {
  //   let usersList = await this.props.model.userModel.getUsers();
  // };

  // /// Save the selected users.
  // saveUsersToService = () => {
  //   console.log("saved.");
  // };

  /// If it exists, retrieves the service name passed through props.
  /// Otherwise, returns a default value.
  getPropsServiceName = () => {
    console.log("service name: ", this.props.serviceName);
    if (this.props.serviceName !== undefined) {
      return this.props.serviceName;
    } else {
      return "";
    }
  };
  /// If it exists, retrieves the URL (SID) passed through props.
  /// Otherwise, returns a default value.
  getPropsURL = () => {
    console.log("url: ", this.props.url);
    if (this.props.url !== undefined) {
      let value = this.props.url;
      return value;
    } else {
      return "";
    }
  };

  ///
  render() {
    // const userList = this.getUsers();
    const serviceName = this.getPropsServiceName(); // this.props.serviceName;
    console.log("service name: ", serviceName);
    const URL = this.getPropsURL();
    console.log("url: ", this.props.url);

    return (
      <div className="base-container">
        <h3>Service info</h3>
        <div className="input-div">
          <Tooltip
            title="Only letters, numbers, spaces, apostrophes, underscores, hyphens."
            placement="right"
            arrow
          >
            <TextField
              id="outlined-basic"
              fullWidth
              label="Service name"
              defaultValue={serviceName}
              variant="outlined"
            />
          </Tooltip>
        </div>

        <div className="input-div">
          <Tooltip
            title="Only letters, numbers, apostrophes, underscores, hyphens. No spaces."
            placement="right"
            arrow
          >
            {/* <TextField
              label="Custom URL"
              id="outlined-start-adornment"
              fullWidth
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">/</InputAdornment>
                )
              }}
              defalutValue={URL}
              variant="outlined"
              inputRef={input => (this.urlInput = input)}
            /> */}
            <TextField
              id="outlined-basic"
              fullWidth
              label="Custom URL"
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">/</InputAdornment>
                )
              }}
              defaultValue={URL}
              variant="outlined"
              inputRef={input => (this.urlInput = input)}
            />
          </Tooltip>
        </div>

        <div className="input-div">
          <ColourSelection onSelect={this.saveColour} />
        </div>

        <h3>Tutors</h3>

        {/* <div className="input-div">{userList}</div> */}

        <Button
          onClick={this.saveService}
          variant="contained"
          color="primary"
          startIcon={<SaveIcon />}
        >
          Save service
        </Button>
      </div>
    );
  }
}
