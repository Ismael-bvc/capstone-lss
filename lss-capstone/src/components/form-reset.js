import React, { Component } from "react";
import { observer } from "mobx-react";

import config from "../config.json";

///
/// Reset Password form.
/// PROPS
///     @model: MainModel which holds every other model.
///     @goback: event handler for clicking back button.
///     @token: reset password token (check if valid).
///   
export default observer(
    class ResetPasswordForm extends Component {
      componentDidMount() {
        // Clear message at the start.
        this.props.model.loginModel.setMessage("");
      }

      // Handler for the back to login button.
      goBack = () => {
        if (this.props.goback) {
          this.props.goback();
        }
      }

      // Handler for change password button.
      changePassword = async (e) => {
        e.preventDefault();
        const token = this.props.token;
        this.props.model.loginModel.changePassword(token);
      }

      /// Callback function for password text change.
      passwordChange = (e) => {
        this.props.model.loginModel.setPassword(e.target.value);
      }

      /// Callback function for confirm password text change.
      confirmChange = (e) => {
        this.props.model.loginModel.setConfirm(e.target.value);
      }

      render() {
        const user = this.props.model.loginModel.getUsername();
        const password = this.props.model.loginModel.getPassword();
        const confirm = this.props.model.loginModel.getConfirm();
        const message = this.props.model.loginModel.getMessage();

        return (
          <div className="login-form">
            <h2>Reset Password</h2>
            <form onSubmit={this.changePassword}>
              <div className="bold">
                {user}  
              </div>
              <div>
                <div>
                  Password
                </div>
                <div>
                  <input type='password' value={password} onChange={this.passwordChange}></input>
                </div>
              </div>
              <div>
                <div>
                  Confirm Password
                </div>
                <div>
                  <input type='password' value={confirm} onChange={this.confirmChange}></input>
                </div>
              </div>
              {
                  message 
                  ? <div className="message-warning center">{message}</div>
                  : <div></div>
              }
              <div>
                <button type="submit" >Change Password</button>
              </div>
            </form>

            <button className="link-button" onClick={this.goBack}>Go to Login</button>
          
          </div>
        );
      }
    }
  );
  