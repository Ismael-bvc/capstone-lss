import React, { Component } from 'react';
import { observer } from 'mobx-react';

///
/// CustomServiceField - displays the field for a certain service
/// - Props
///     model - model that is of ModelServiceObject class

export default observer(
    class CustomServiceField extends Component{
        render() {
            const code = this.props.model.getCode();
            const pass = this.props.model.loginModel.getPassword();
            const message = this.props.model.loginModel.getMessage();

            return(
                <div>
                    <h2>Login</h2>
                    <form onSubmit={this.loginClick}>
                        <div>
                            Username
                            <input type='text' value={user} onChange={this.userChange}></input>
                        </div>
                        <div>
                            Password
                            <input type='password' value={pass} onChange={this.passwordChange}></input>
                        </div>
                        {
                            message 
                            ? <div className="message-warning">{message}</div>
                            : <div></div>
                        }
                        
                        <div>
                            <button type="submit">Login</button>
                        </div>
                        
                    </form>
                    <button className="link-button" onClick={this.forgot}>Forgot Password?</button>
                </div>
            );
        }
    }
);