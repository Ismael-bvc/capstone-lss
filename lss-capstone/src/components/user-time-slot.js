import React, { Component } from "react";
import { observer } from "mobx-react";

import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";

export default observer(
class AddTimeSlot extends Component {
  constructor(props) {
    super(props);
  }

  timeSlot = () => {
    let slot = [];
    let list = this.props.props.model.userModel.getListOfTime();

   for (let i = 0; i < list.length; i++){
    var [sh,sm] = list[i].start.split(":");
    var [eh,em] = list[i].end.split(":");
      let start = sh;
      let startAMPM = "AM";
      let end = eh;
      let endAMPM = "AM";

      if(start > 12){
        start -= 12;
        startAMPM = "PM";
      }
      if(end > 12)
      {
        end -= 12;
        endAMPM = "PM"
      }
      slot.push(<TableRow><TableCell>From: {start+":"+sm+"  "+startAMPM} To: {end+":"+em+"  "+endAMPM}</TableCell><TableCell>
        <button onClick={this.deleteTimeSlot} id={list[i].id}>Delete</button></TableCell></TableRow>)
    };
    
    return slot;
  };
  deleteTimeSlot = () =>
  {
    this.props.callbackSlot()
  }

  render() {

    return (<>{this.timeSlot()}</>);
  }
});
