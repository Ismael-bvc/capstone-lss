import React, { Component } from 'react';
import { observer } from 'mobx-react';

///
/// FormLogin - Login Form with username and password
/// - Props
///     model - model for the whole application
///     forgot - callback function to go go forgot page

export default observer(
  class FormLogin extends Component{
    componentDidMount() {
      this.props.model.loginModel.setMessage("");
    }

    forgot = (e) => {
      e.preventDefault();
      if (this.props.forgot) {
        this.props.forgot();
      }
    }

    /// Callback function for username text change.
    userChange = (e) => {
      this.props.model.loginModel.setUsername(e.target.value);
    }

    /// Callback function for password text change.
    passwordChange = (e) => {
      this.props.model.loginModel.setPassword(e.target.value);
    }

    /// Function for login button click.
    loginClick = async (e) => {
      // Prevent form submit.
      e.preventDefault();
      
      // Get JWT token from server.
      const token = await this.props.model.loginModel.login();
      if (token) {
        // Set credetials based on token.
        if (this.props.model.setCredentials(token)) {
          // Save token to local storage
          const key = this.props.model.loginModel.getTokenKey();    
          localStorage.setItem(key, token);
        }
        else {
          this.props.model.loginModel.setMessage("Error logging in.");
        }
      }
    }

        ///
    render() {
      const user = this.props.model.loginModel.getUsername();
      const pass = this.props.model.loginModel.getPassword();
      const message = this.props.model.loginModel.getMessage();

      return(
        <div className="login-form" >
          
          <h2>Login</h2>
          
          <form onSubmit={this.loginClick}>
            <div>
              <div>
                Username
              </div>
              <div>
                <input type='text' value={user} onChange={this.userChange}></input>
              </div>
            </div>
            <div>
              <div>
                Password
              </div>
              <div>
                <input type='password' value={pass} onChange={this.passwordChange}></input>
              </div>
            </div>
            {
              message 
              ? <div className="message-warning">{message}</div>
              : <div></div>
            }
            
            <div>
              <button type="submit">Login</button>
            </div>
              
          </form>
          
          <button className="link-button" onClick={this.forgot}>Forgot Password?</button>
        
        </div>
      );
    }
  }
);