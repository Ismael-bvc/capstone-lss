import React, { Component } from "react";
import { observer } from "mobx-react";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TablePagination from "@material-ui/core/TablePagination";
import TableRow from "@material-ui/core/TableRow";
import { Link } from "react-router-dom";
import Grid from "@material-ui/core/Grid";
import SearchIcon from "@material-ui/icons/Search";
import TextField from "@material-ui/core/TextField";

/** PROPS
 * @list: array of objects
 */
export default observer(
  class DisplayServiceRow extends Component {
    findService = event => {
      if (event.target.value === "") {
        this.props.model.serviceModel.getServicesData();
      }
      this.props.model.serviceModel.setfindService(event.target.value);
    };

    serviceRows = () => {
      //let findRole = this.props.model.roleModel.getfindRole();
      let page = 1; //this.props.props.model.roleModel.getPageNumber();
      let rowperpage = this.props.model.serviceModel.getPageRow(); //this.props.props.model.roleModel.getPageRow();
      let services = this.props.model.serviceModel.getServicesList();
      console.log(services);
      let row = [];

      // if (services.length > 0) {
      //   for (
      //     let i = (page - 1) * rowperpage;
      //     i < (page - 1) * rowperpage + rowperpage;
      //     i++
      //   ) {
      //    if (services[i] === undefined) continue;
      // for(let i = 0; i<services.length; i++)

      {
        services.map(s => {
          //console.log(servicesArray);
          const url = "/service/" + s.getCode();
          row.push(
            <TableContainer>
              <Table>
                <TableHead>
                  <TableRow>
                    <TableCell key={s.getOrder()}>
                      <Link to={url}>{s.getDesc()}</Link>
                    </TableCell>
                  </TableRow>
                </TableHead>
              </Table>
            </TableContainer>
          );
        });
      }
      //   row.push(
      //     <TableRow>
      //       <TableCell>{}</TableCell>
      //     </TableRow>
      //   );
      //    }
      //  } else
      // row.push(
      //   <TableRow>
      //     <TableCell>No Result found</TableCell>
      //   </TableRow>
      // );

      return row;
    };

    render() {
      return (
        <>
          <TableContainer>
            <Table>
              <TableHead>{this.serviceRows()}</TableHead>
            </Table>
          </TableContainer>
        </>
      );
    }
  }
);
