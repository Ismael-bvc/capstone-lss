import React, { Component } from "react";

import { observer } from "mobx-react";

import PropTypes from "prop-types";

import { withStyles } from "@material-ui/core/styles";

import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import Button from "@material-ui/core/Button";

import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";

import SchedulerWeekGrid from "./buttons-setAppointmentDay";
import SchedulerTimeGrid from "./buttons-setAppointmentTime";
import SchedulerTuteeForm from "./form-setAppointmentTutee";

import moment from "moment";

import { toJS } from "mobx";

const styles = theme => ({
  table: { minWidth: 650 },
  root: {
    flexGrow: 1,
    width: "75%",
    padding: theme.spacing(2)
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: "center",
    color: theme.palette.text.secondary
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular
  }
});

const AppointmentCalendar = observer(
  class AppointmentCalendar extends Component {
    constructor(props) {
      super(props);

      this.state = {
        isOpenDatePanel: true,
        isOpenTimePanel: false,
        isOpenFormPanel: false
      };
    }

    async componentDidMount() {
      await this.props.model.tutorModel.fetchTutorData(2); // 2 is a placeholder value. 2 should be a variable.
      // console.log(toJS(this.props.model.tutorModel.currentWeekSchedule));
      // console.log(toJS(this.props.model.tutorModel.nextWeekSchedule));
      // console.log(toJS(this.props.model.tutorModel.selectedDaySchedule));
    }

    handleDatePanel = () => {
      if (this.state.isOpenDatePanel) {
        this.setState({ isOpenDatePanel: false });
      } else {
        this.setState({ isOpenDatePanel: true });
      }
    };

    handleTimePanel = () => {
      if (this.state.isOpenTimePanel) {
        this.setState({ isOpenTimePanel: false });
      } else {
        this.setState({ isOpenTimePanel: true });
      }
    };

    handleFormPanel = () => {
      if (this.state.isOpenFormPanel) {
        this.setState({ isOpenFormPanel: false });
      } else {
        this.setState({ isOpenFormPanel: true });
      }
    };

    shouldOpenTimePanel = async (willOpen, month, date, day, isCurrentWeek) => {
      const { tutorModel, appointmentModel } = this.props.model;

      if (willOpen) {
        //
        // Fetch tutor's appointment schedule for the day that was selected.
        await tutorModel.getAvailableDayTimes(day);

        //
        // Set appointment data. The data set for this model will be used by the tutee
        // and tutor models.
        appointmentModel.setMonth(month);
        appointmentModel.setDate(date);
        appointmentModel.setDay(day);
        appointmentModel.setIsCurrentWeek(isCurrentWeek);
        // console.log(appointmentModel.getAppointment());

        this.setState({ isOpenTimePanel: willOpen });
        this.setState({ isOpenDatePanel: !willOpen });
      }
    };

    shouldOpenFormPanel = (willOpen, startTime) => {
      const { appointmentModel } = this.props.model;

      // console.log(startTime);

      if (willOpen) {
        appointmentModel.setStartTime(startTime);
        // console.log(appointmentModel.getAppointment());

        this.setState({ isOpenTimePanel: !willOpen });
        this.setState({ isOpenFormPanel: willOpen });
      }
    };

    handleFormSubmit = (name, email, phone, specialRequest) => {
      const { appointmentModel, tutorModel, tuteeModel } = this.props.model;

      try {
        // appointmentModel.setTuteeName(name);
        // appointmentModel.setTuteeEmail(email);
        // appointmentModel.setTuteePhone(phone);
        // appointmentModel.setSpecialRequest(specialRequest);

        // const tutorId = tutorModel.getTutorId();
        // const tutorName = tutorModel.getTutorName();
        const startTime = appointmentModel.getStartTime();
        const day = appointmentModel.getDay();
        const month = appointmentModel.getMonth();
        const date = appointmentModel.getDate();
        const isCurrentWeek = appointmentModel.getIsCurrentWeek();

        //
        // Appointment payload for tutee's appointments.
        // tuteeModel.setAppointment({
        //   month: month,
        //   date: date,
        //   day: day,
        //   startTime: startTime,
        //   tutorId: tutorId,
        //   tutorName: tutorName
        // });

        // console.log(`tutee appointment object ->`);
        // console.log(toJS(tuteeModel.getAppointments()));

        //
        // Appointment payload for tutor's appointments.
        tutorModel.setAppointment({
          month: month,
          date: date,
          day: day,
          startTime: startTime,
          isCurrentWeek: isCurrentWeek,
          name: name,
          email: email,
          phone: phone,
          specialRequest: specialRequest
        });

        // console.log(`tutor appointment object ->`);
        // console.log(toJS(tutorModel.getCurrentWeekAppointments()));
      } catch (e) {
        console.log(`Error -> ${e}, Email: ${email}`);
      }
    };

    render() {
      const { classes } = this.props;

      console.log(this.props.model.appointmentModel.getAppointment());

      return (
        <Container className={classes.root}>
          <ExpansionPanel
            name="datePanel"
            onChange={this.handleDatePanel}
            expanded={this.state.isOpenDatePanel}
          >
            <ExpansionPanelSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls="panel1a-content"
              id="panel1a-header"
            >
              <Typography className={classes.heading}>Select a date</Typography>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails>
              <Grid container spacing={3}>
                <SchedulerWeekGrid
                  currentWeek={this.props.model.tutorModel.getCurrentWeek}
                  nextWeek={this.props.model.tutorModel.getNextWeek}
                  onOpenTimePanel={this.shouldOpenTimePanel}
                />
              </Grid>
            </ExpansionPanelDetails>
          </ExpansionPanel>
          <ExpansionPanel
            name="timePanel"
            onChange={this.handleTimePanel}
            expanded={this.state.isOpenTimePanel}
          >
            <ExpansionPanelSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls="panel1a-content"
              id="panel1a-header"
            >
              <Typography className={classes.heading}>Select a time</Typography>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails>
              <Grid container spacing={3}>
                <SchedulerTimeGrid
                  tutorTimes={
                    this.props.model.tutorModel.selectedDaySchedule
                      ? this.props.model.tutorModel.selectedDaySchedule
                      : null
                  }
                  onOpenFormPanel={this.shouldOpenFormPanel}
                />
              </Grid>
            </ExpansionPanelDetails>
          </ExpansionPanel>
          <ExpansionPanel
            name="formPanel"
            onChange={this.handleFormPanel}
            expanded={this.state.isOpenFormPanel}
          >
            <ExpansionPanelSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls="panel1a-content"
              id="panel3a-header"
            >
              <Typography className={classes.heading}>Fill form</Typography>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails>
              <Grid container spacing={3}>
                <SchedulerTuteeForm
                  onSetAppointment={this.handleFormSubmit}
                  model={this.props.model}
                />
              </Grid>
            </ExpansionPanelDetails>
          </ExpansionPanel>
        </Container>
      );
    }
  }
);

AppointmentCalendar.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(AppointmentCalendar);
