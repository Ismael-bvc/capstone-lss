import React, { Component } from "react";

import WaitlistTableRow from "./WaitlistTableRow";

import Container from "@material-ui/core/Container";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableBody from "@material-ui/core/TableBody";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import Typography from "@material-ui/core/Typography";

/** PROPS
 * @list: an array of objects
 */

class TutorWaitlist extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    return (
      <Container>
        <Table responsive striped hover size="sm">
          <TableHead>
            <TableRow>
              <TableCell>
                <Typography variant="h6">Queue</Typography>
              </TableCell>
              <TableCell>
                <Typography variant="h6">Name</Typography>
              </TableCell>
              <TableCell>
                <Typography variant="h6">Status</Typography>
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            <WaitlistTableRow tutees={this.props.list} />
          </TableBody>
        </Table>
      </Container>
    );
  }
}
export default TutorWaitlist;
