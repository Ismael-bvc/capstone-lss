import React, { Component } from "react";
import { observer } from "mobx-react";

//Add tutor
export default observer(
  class Addtutor extends Component {
    constructor(props) {
      super(props);
      this.mailSent = false;
    }

    emailChange = e => {
      this.props.model.addTutor.setTutorEmail(e.target.value);
    };

    onClickSend = e => {
      e.preventDefault();
      this.mailSent = this.props.model.addTutor.onSendInvite();
    };

    render() {
      const email = this.props.model.addTutor.getTutorEmail();

      return (
        <div align="center">
          <div className="Invitetutor">
            <form>
              <h2>Invite a User:</h2>
              <div className="Tutor">
                <label>
                  Send invitation to :
                  <input
                    type="text"
                    value={email}
                    placeholder="Email address"
                    onChange={this.emailChange}
                  />
                </label>
                <br />
                <button type="text" className="" onClick={this.onClickSend}>
                  Send Invite
                </button>
                {this.mailSent && alert("mail sent")}
              </div>
            </form>
          </div>
        </div>
      );
    }
  }
);
