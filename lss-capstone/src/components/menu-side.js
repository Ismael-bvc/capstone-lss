import React, { Component } from "react";
import { observer } from "mobx-react";

import { Link } from "react-router-dom";

import "../styles/menu-side.css";

/* Material-UI stuff */
import clsx from 'clsx';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import MailIcon from '@material-ui/icons/Mail';

const drawerWidth = 240;

const classes = theme => ({
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  hide: {
    display: 'none',
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
    whiteSpace: 'nowrap',
  },
  drawerOpen: {
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerClose: {
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    overflowX: 'hidden',
    width: theme.spacing(7) + 1,
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing(9) + 1,
    },
  },
  toolbar: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: theme.spacing(0, 1),
    ...theme.mixins.toolbar,
  },
  
});

function ListItemLink(props) {
  const { icon, primary, to } = props;

  const renderLink = React.useMemo(
    () => React.forwardRef((itemProps, ref) => <Link to={to} ref={ref} {...itemProps} />),
    [to],
  );

  return (
    <li>
      <ListItem button component={renderLink}>
        {icon ? <ListItemIcon>{icon}</ListItemIcon> : null}
        <ListItemText primary={primary} />
      </ListItem>
    </li>
  );
}

/*
ListItemLink.propTypes = {
  icon: PropTypes.element,
  primary: PropTypes.string.isRequired,
  to: PropTypes.string.isRequired,
};
*/

export default observer(
    class SideMenu extends Component {
      constructor(props) {
        super(props);

        this.state = {
          open: false,
          setOpen: false
        };
      }

      handleDrawerOpen = () => {
        this.setState({setOpen: true});
      }

      handleDrawerClose = () => {
        this.setState({setOpen: false})
      }

      logout =  (e) => {
        e.preventDefault();
        if(this.props.logout) {
          this.props.logout();
        }
      }

      render() {
        const canDashboard = this.props.model.getCanDashboard();
        const canReports = this.props.model.getCanReports();
        const canServices = this.props.model.getCanServices();
        const canRoles = this.props.model.getCanRoles();
        const canUsers = this.props.model.getCanUsers();
        const canAdmin = this.props.model.getCanAdmin();
        const isLogged = this.props.model.isLogged();

        return (
          isLogged 
          ?
            <div className="menu-side">
              <Drawer
                variant="permanent"
                className={clsx(classes.drawer, {
                  [classes.drawerOpen]: this.state.open,
                  [classes.drawerClose]: !this.state.open,
                })}
                classes={{
                  paper: clsx({
                    [classes.drawerOpen]: this.open,
                    [classes.drawerClose]: !this.state.open,
                  }),
                }}
              >
                <div className={classes.toolbar}>
                  <IconButton onClick={this.handleDrawerClose}>
                    <ChevronRightIcon />
                  </IconButton>
                </div>
                <Divider />
                <List>
                    
                  {canDashboard && (                    
                    <ListItemLink to="/" primary="Dashboard" icon={<InboxIcon />} />
                  )}

                  {canReports && (
                    <ListItemLink to="/reports" primary="Analytics" icon={<InboxIcon />} />
                  )}

                  {canServices && (
                    <ListItemLink to="/services" primary="Services" icon={<InboxIcon />} />
                  )}

                  {canRoles && (
                    <ListItemLink to="/roles" primary="Roles" icon={<InboxIcon />} />
                  )}

                  {canUsers && (
                    <ListItemLink to="/users" primary="Users" icon={<InboxIcon />} />
                  )}

                  {canAdmin && (
                    <ListItemLink to="/admin" primary="Admin" icon={<InboxIcon />} />
                  )}
                </List>

                <Divider />

                <List>
                  
                  {canDashboard && (
                    <ListItemLink to="/profile" primary="Profile" icon={<InboxIcon />} />
                  )}

                  <ListItem button key="Logout">
                    <ListItemIcon><InboxIcon /></ListItemIcon>
                    <ListItemText primary={<button className="link-button" onClick={this.logout}>Logout</button>} />
                  </ListItem>
                </List>

              </Drawer>
            </div>
          :
            <div></div>
        )
      }
    }
  );
  