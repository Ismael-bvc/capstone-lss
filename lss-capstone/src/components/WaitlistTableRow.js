import React, { Component } from "react";

import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";

/** PROPS
 * @list: array of objects
 */

class WaitlistTableRow extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  tableRows = () => {
    if (this.props.tutees) {
      let row = this.props.tutees.map((tutee, index) => (
        <TableRow key={index} id={tutee.blockId} hover>
          <TableCell>{tutee.queue}</TableCell>
          <TableCell>{tutee.name}</TableCell>
          <TableCell>{tutee.status}</TableCell>
        </TableRow>
      ));
      return row;
    }
  };

  render() {
    return <>{this.tableRows()}</>;
  }
}
export default WaitlistTableRow;
