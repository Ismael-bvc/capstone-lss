import React, { Component } from "react";
import { observer } from "mobx-react";

import config from "../config.json";

///
/// Forgot Password form.
/// PROPS
///     @model: MainModel which holds every other model.
///     @goback: event handler for clicking back button.
///   
export default observer(
    class ForgotPasswordForm extends Component {
      componentDidMount() {
        this.props.model.loginModel.setMessage("");
      }

      goBack = () => {
        if (this.props.goback) {
          this.props.goback();
        }
      }

      sendEmail = async (e) => {
        e.preventDefault();
        this.props.model.loginModel.resetPassword();
      }

      /// Callback function for username text change.
      userChange = (e) => {
        this.props.model.loginModel.setUsername(e.target.value);
      }

      render() {
        const user = this.props.model.loginModel.getUsername();
        const message = this.props.model.loginModel.getMessage();

        return (
          <div className="login-form">

            <h2>Forgot Password?</h2>
            
            <form onSubmit={this.sendEmail}>
              <div>
                <div>
                  Email/Username
                </div>
                <div>
                  <input type='text' value={user} onChange={this.userChange}></input>
                </div>
              </div>
              {
                  message 
                  ? <div className="message-warning">{message}</div>
                  : <div></div>
              }
              <div>
                <button type="submit">Reset Password</button>
              </div>
            </form>
          
            <button className="link-button" onClick={this.goBack}>Back</button>
          
          </div>
        );
      }
    }
  );
  