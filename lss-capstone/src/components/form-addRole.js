import React, { Component } from "react";

// import ".././styles/form-addRole.css";

import Button from "@material-ui/core/Button";
import SaveIcon from "@material-ui/icons/Save";
import InputLabel from "@material-ui/core/InputLabel";
import FormControl from "@material-ui/core/FormControl";
import Checkbox from "@material-ui/core/Checkbox";
import OutlinedInput from "@material-ui/core/OutlinedInput";
import FormGroup from "@material-ui/core/FormGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormLabel from "@material-ui/core/FormLabel";
import { toast } from "react-toastify";

///
/// Props:
///   model: MainModel which holds every other model.
///
export class AddRole extends Component {
  /// Save the role into the database.
  saveRole = () => {
    // Generate a SID:
    this.props.model.roleModel.setCode(
      this.props.model.generateSid(this.props.model.roleModel.getDesc())
    );

    // Ensure fields have been filled first.
    if (this.props.model.roleModel.getDesc() === "") {
      // If not, send warning toast.
      toast.warn("Please ensure all fields are entered correctly.", {
        className: "toast-warning toast-base"
      });
    } else {
      // Else, save the info.
      if (this.props.model.roleModel.saveRoleData()) {
        toast.success("Role successfully saved.", {
          className: "toast-success toast-base"
        });
      } else {
        toast.error("An error ocurred. Role could not be saved.", {
          className: "toast-failure toast-base"
        });
      }
    }
  };

  /// Save the name of the new role into the model.
  saveRoleName = () => {
    this.props.model.roleModel.setDesc(this.roleTitleInput.value);
  };

  /// Save the array of privliges into the model.
  /// Params:
  ///   value: the privlige to push into the model's privileges array.
  savePrivileges = value => {
    this.props.model.roleModel.setPrivs(value);
  };
  /// Saves the array of services into the model.
  /// Params:
  ///   value: the services to push into the model's services array.
  saveServices = value => {
    this.props.model.roleModel.setServs(value);
  };

  /// Retreive all services to put into an array of checkboxes to be displayed.
  getServicesChecklist = () => {
    // Retreive all services from the backend.
    // Put services into array of MUI checkboxes.
    let services = [];
    services.push(this.props.model.getServices());

    let formArray = [];
    formArray.forEach(
      element =>
        function() {
          formArray.push(
            <FormControlLabel
              control={<Checkbox value={formArray[element]} />}
              label={formArray[element]}
              onChange={e => this.saveServices(e.target.value)}
            />
          );
        }
    );
  };

  ///
  render() {
    const availableServices = this.getServicesChecklist();

    return (
      <div className="addRoles-base-container">
        <FormControl variant="outlined">
          <InputLabel htmlFor="component-outlined">Role title</InputLabel>
          <OutlinedInput
            id="component-outlined"
            label="Role title"
            inputRef={input => (this.roleTitleInput = input)}
            onChange={this.saveRoleName}
          />
        </FormControl>

        <br />

        <FormControl component="fieldset">
          <FormLabel component="legend">Privileges</FormLabel>
          <FormGroup row>
            <FormControlLabel
              control={<Checkbox value="Privilege 1" />}
              label="Privilege 1"
              onChange={e => this.savePrivileges(e.currentTarget.value)}
            />
            <FormControlLabel
              control={<Checkbox value="Privilege 2" />}
              label="Privilege 2"
              onChange={e => this.savePrivileges(e.currentTarget.value)}
            />
            <FormControlLabel
              control={<Checkbox value="Privilege 3" />}
              label="Privilege 3"
              onChange={e => this.savePrivileges(e.currentTarget.value)}
            />
            <FormControlLabel
              control={<Checkbox value="Privilege 4" />}
              label="Privilege 4"
              onChange={e => this.savePrivileges(e.currentTarget.value)}
            />
            <FormControlLabel
              control={<Checkbox value="Privilege 5" />}
              label="Privilege 5"
              onChange={e => this.savePrivileges(e.currentTarget.value)}
            />
          </FormGroup>
        </FormControl>

        <br />

        <FormControl component="fieldset">
          <FormLabel component="legend">Services</FormLabel>
          <FormGroup row>
            <FormControlLabel
              control={<Checkbox value="*" />}
              label="Select all"
              onChange={e => this.saveServices(e.target.value)}
            />
            {availableServices}
          </FormGroup>
        </FormControl>

        <br />

        <Button
          onClick={this.saveRole}
          variant="contained"
          color="primary"
          startIcon={<SaveIcon />}
        >
          Save role
        </Button>
      </div>
    );
  }
}
