import React, { Component } from "react";
import { observer } from "mobx-react";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkboxx from "@material-ui/core/Checkbox";
export default observer(
  class Checkbox extends Component {
    handleChange = value => {
      console.log("cheked " + value);
      this.props.props.model.displayModel.CheckboxStateChanger(value);
    };

    checkBoxs = () => {
      let NumberOfCourses = this.props.props.model.displayModel.GetNumberOfCourses();
      let courses = this.props.props.model.displayModel.GetCoursesforCheckbox();
      let elememnt = [];

      for (let i = 0; i < NumberOfCourses; i++) {
        elememnt.push(
          <FormControlLabel
            control={
              <Checkboxx
                checked={courses[i].state}
                onChange={() => this.handleChange(courses[i].name)}
                value={"checkedB" + i}
                color="primary"
              />
            }
            label={courses[i].name}
          />
        );
      }
      return elememnt;
    };
    ChangeCaroucelScene = () => {
      this.props.props.model.displayModel.SetScenToCarousel(true);
    };

    render() {
      return (
        <>
          {this.checkBoxs()}
          <button className="oneButton" onClick={this.props.onClick}>
            Rotate
          </button>
        </>
      );
    }
  }
);
