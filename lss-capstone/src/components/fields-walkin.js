import React, { Component } from "react";
import { observer } from "mobx-react";
import TextField from "@material-ui/core/TextField";
import { makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import Checkbox from "@material-ui/core/Checkbox";
import FormControlLabel from "@material-ui/core/FormControlLabel";

//date/time picker
import "date-fns";
import Grid from "@material-ui/core/Grid";
import DateFnsUtils from "@date-io/date-fns";
import {
  MuiPickersUtilsProvider,
  KeyboardTimePicker,
  KeyboardDatePicker
} from "@material-ui/pickers";

// const useStyles = makeStyles(theme => ({
//   root: {
//     "& > *": {
//       margin: theme.spacing(1),
//       width: 200
//     }
//   }
// }));

///
/// CustomServiceField - displays the field for a certain service
/// - Props
///     model - model that is of ModelServiceObject class

export default observer(
  class WalkInField extends Component {
    createField = type => {
      if (type === "text")
        return (
          <form>
            <TextField
              required
              id="outlined-required"
              //   variant="outlined"
              type="text"
              label="Full Name"
              style={{
                width: "50%"
              }}
            ></TextField>
          </form>
        );
      else if (type === "number")
        return (
          <TextField
            required
            id="filled-required"
            type="number"
            // variant="outlined"
            label="Student ID"
            onInput={e => {
              e.target.value = Math.max(0, parseInt(e.target.value))
                .toString()
                .slice(0, 6);
            }}
            style={{
              width: "50%"
            }}
          ></TextField>
        );
      else if (type === "phone")
        return (
          <TextField
            id="standard-basic"
            type="tel"
            // variant="outlined"
            label="Phone Number"
            style={{
              width: "50%"
            }}
          ></TextField>
        );
      else if (type === "email")
        return (
          <TextField
            required
            id="outlined-required"
            type="email"
            // variant="outlined"
            label="Email"
            style={{
              width: "50%"
            }}
          ></TextField>
        );
      else if (type === "multitext")
        return (
          <TextField
            id="outline-multi"
            variant="outlined"
            multiline
            rows="4"
            label="Notes"
            style={{
              width: "50%"
            }}
          ></TextField>
        );
      else if (type === "checkbox")
        return (
          <FormControlLabel
            control={<Checkbox color="primary" />}
            label="CheckBox"
            labelPlacement="start"
          />
        );
      else if (type === "date")
        return (
          <MuiPickersUtilsProvider utils={DateFnsUtils}>
            <KeyboardDatePicker
              disableToolbar
              variant="inline"
              format="MM/dd/yyyy"
              margin="normal"
              id="date-picker-inline"
              label="Date Picker"
              labelPlacement="start"
              KeyboardButtonProps={{
                "aria-label": "change date"
              }}
            />
          </MuiPickersUtilsProvider>
        );
      else if (type === "time")
        return (
          <MuiPickersUtilsProvider utils={DateFnsUtils}>
            <KeyboardTimePicker
              margin="normal"
              id="time-picker"
              label="Time picker"
              KeyboardButtonProps={{
                "aria-label": "change time"
              }}
            />
          </MuiPickersUtilsProvider>
        );
      else return null;
    };

    render() {
      const code = this.props.model.getCode();
      const desc = this.props.model.getDesc();
      const enabled = this.props.model.getEnabled();
      const type = this.props.model.getType();
      return enabled ? (
        <div>
          <Container
            style={{
              //backgroundColor: "#dddddd",
              paddingLeft: "29%"
            }}
            maxWidth="lg"
          >
            <Typography
              component="div"
              style={{
                // backgroundColor: "#eeeeee",
                height: "50px",
                paddingRight: "100vh"
              }}
            />
            {/* <span>{desc}</span> */}
            {this.createField(type)}
          </Container>
        </div>
      ) : null;
      //   if (enabled === true) {
      //     return <div>{this.createField(type)}</div>;
      //   }
    }
  }
);
