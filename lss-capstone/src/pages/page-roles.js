import React, { Component } from "react";
import { observer } from "mobx-react";
import { Link } from "react-router-dom";
import TablePagination from "@material-ui/core/TablePagination";
import TableRow from "@material-ui/core/TableRow";
import TableFooter from "@material-ui/core/TableFooter";
import SearchIcon from "@material-ui/icons/Search";

import { makeStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Grid from "@material-ui/core/Grid";

import DisplayRoleRow from "../components/RolesTableRow.js";
///
/// Displays roles.
///
/// Props:
///     model: MainModel which holds every other model.
///

export default observer(
  class RolesPage extends Component {
    async componentDidMount() {
      //console.log("calling");
      await this.props.model.roleModel.getRoleData();
    }
    loadPreviousPage = () => {
      this.props.model.roleModel.setPageNumber(-1);
    };
    loadNextPage = () => {
      this.props.model.roleModel.setPageNumber(1);
    };
    handleChangeRowsPerPage = event => {
      this.props.model.roleModel.setPageRow(event.target.value, 10);
    };

    findRole = event => {
      if (event.target.value === "") {
        this.props.model.roleModel.GetRoleData();
      }
      this.props.model.roleModel.setfindRole(event.target.value);
    };

    render() {
      const canRoles = this.props.model.getCanRoles();

      //const canRoles = this.props.model.getCanRoles();
      const page = this.props.model.roleModel.getPageNumber();
      let roles = this.props.model.roleModel.getRoles();
      let row = this.props.model.roleModel.getPageRow();
      let maxPage = Math.ceil(roles.length / row);

      if (roles.length < row) {
        maxPage = 1;
      }

      let max = false;
      let min = false;

      if (page < 2) min = true;
      else min = false;
      if (page >= Math.ceil(roles.length / row)) max = true;
      else max = false;
      //console.log(this.props.model.roleModel);

      return (
        <div>
          <h1>Roles</h1>
          {canRoles && (
            <div>
              <Link to="/role">Add Role</Link>
            </div>
          )}

          <div>
            List of roles here
            <ul>
              <li>sample 1</li>
              <li>sample 2</li>
              <li>sample 3</li>
              <li>sample 4</li>
            </ul>
            <TableFooter>
              <TableRow>
                <TablePagination
                  rowsPerPage="3"
                  labelRowsPerPage="Row per page"
                  labelDisplayedRows={({ from, to, count }) =>
                    `${page}-${maxPage}`
                  }
                  page={page}
                  rowsPerPageOptions={[5, 10, 20, { label: "All", value: -1 }]}
                  onChangeRowsPerPage={this.handleChangeRowsPerPage}
                  backIconButtonProps={{
                    "aria-label": "Previous Page",
                    onClick: this.loadPreviousPage,
                    disabled: min
                  }}
                  nextIconButtonProps={{
                    "aria-label": "Next Page",
                    onClick: this.loadNextPage,
                    disabled: max
                  }}
                ></TablePagination>
              </TableRow>
              <DisplayRoleRow props={this.props} />
              <div>
                <Grid container spacing={1} alignItems="flex-end">
                  <Grid item>
                    <SearchIcon />
                  </Grid>
                  <Grid item>
                    <TextField
                      id="input-with-icon-grid"
                      label="Find role"
                      onChange={this.findRole}
                    />
                  </Grid>
                </Grid>
              </div>
            </TableFooter>
          </div>
        </div>
      );
    }
  }
);
