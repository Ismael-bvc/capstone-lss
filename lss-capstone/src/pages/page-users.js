import React, { Component } from "react";
import { observer } from "mobx-react";
import { Link } from "react-router-dom";

import Typography from "@material-ui/core/Typography";

///
/// This is the page that will serve as "home" for the currently logged user.
///
/// PROPS
///     @model: MainModel which holds every other model.
///
export default observer(
  class UsersPage extends Component {
    render() {
      const canUsers = this.props.model.getCanUsers();

      return (
        <div>
          <h1>Users</h1>
            {canUsers && (
                <div>
                  <Link to="/user">Invite User</Link>
                </div>
            )}
            <div>
              List of users here
              <ul>
                <li><Link to="/user/sample">sample 1</Link></li>
                <li>sample 2</li>
                <li>sample 3</li>
                <li>sample 4</li>
              </ul>
            </div>
        </div>
      );
    }
  }
);
