import React, { Component } from "react";
import { observer } from "mobx-react";
import { Link } from "react-router-dom";

import { Service } from "../components/form-service.js";

///
/// This is the page that will set the fields that will appear when booking.
///
/// Props:
///     model: MainModel which holds every other model.
///
export default observer(
  class AddServicePage extends Component {
    render() {
      const canServices = this.props.model.getCanServices();

      return (
        <div>
          {canServices && (
            <div>
              <Link to="/services">Back to Services</Link>
            </div>
          )}
          <h1>Add Service</h1>

          <Service model={this.props.model} />
        </div>
      );
    }
  }
);
