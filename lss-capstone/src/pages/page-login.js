import React, { Component } from "react";
import { observer } from "mobx-react";
import { Link } from "react-router-dom";

import LoginForm from '../components/form-login.js';

///
/// This is the page for logging in. Will show up if user doesnt have token..
///
/// PROPS
///     @model: MainModel which holds every other model.
///
export default observer(
    class LoginPage extends Component {
      forgotPassword = () => {
        this.props.history.push("/forgot");
      }

      render() {
        return (
          <div className="page">
            <Link to="/offeredservices">Offered Services</Link>
            <LoginForm model={this.props.model} forgot={this.forgotPassword} />
          </div>
        );
      }
    }
  );
  