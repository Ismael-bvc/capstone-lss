import React, { Component, Fragment } from "react";

import { observer } from "mobx-react";

import AddTimeSlot from "../components/user-time-slot"

import TextField from '@material-ui/core/TextField';
import DisplayTable from "../components/TutorWaitlistForCarousel";
import Container from "@material-ui/core/Container";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";

export default observer(
class UserSchedule extends Component {
  constructor(props) {
    super(props);
  }
  async componentDidMount() {
    const {sid} = this.props.match.params;
    await this.props.model.userModel.setUserName(sid);
    await this.props.model.userModel.fetchUserTimeSlots();
  }
  saveStartTime = event =>{
    var t = event.target.value;
    var [h,m] = t.split(":");
    this.props.model.userModel.setStartTime(h, m);
  }
  saveEndTime = event =>{
    var t = event.target.value;
    var [h,m] = t.split(":");
    this.props.model.userModel.setEndTime(h, m);
  }
  submitInfo = async ()=>{
    await this.props.model.userModel.addTime("Monday");
    await this.props.model.userModel.fetchUserTimeSlots();
  }
  deleteTimeSlot = async (value)=>{
    console.log(value);
  }

  render() {
    

    return (
      <div>
        <div>
          <TextField
            id="from"
            label="Start"
            type="time"
            onChange = {this.saveStartTime}
            defaultValue="05:00"
            InputLabelProps={{
              shrink: true,
            }}
            inputProps={{
              step: 300, // 5 min
            }}
          />
          <h2> to </h2>
          <TextField
            id="to"
            label="End"
            type="time"
            onChange = {this.saveEndTime}
            defaultValue="05:30"
            InputLabelProps={{
              shrink: true,
            }}
            inputProps={{
              step: 300, // 5 min
            }}
          />
        </div>
          <button type="submit" onClick={()=>this.submitInfo()}>Add</button>
         <AddTimeSlot props={this.props} onClick = {()=>this.deleteTimeSlot()} callbackSlot={()=>this.deleteTimeSlot()} />
      </div>
    );
  }
});
