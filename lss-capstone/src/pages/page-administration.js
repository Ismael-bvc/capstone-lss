import React, { Component } from "react";
import { observer } from "mobx-react";
import { Link } from "react-router-dom";

import Typography from "@material-ui/core/Typography";

///
/// This is the page that will serve as "home" for the currently logged user.
///
/// PROPS
///     @model: MainModel which holds every other model.
///
export default observer(
  class AdministratonPage extends Component {
    render() {
      const canAdmin = this.props.model.getCanAdmin();
      
      return (
        <div>
          <h1>Administration</h1>
          
        </div>
      );
    }
  }
);
