import React, { Component } from "react";
import { observer } from "mobx-react";
import config  from "../config.json";
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import MailIcon from '@material-ui/icons/Mail';
import Button from '@material-ui/core/Button';

const useStyles = makeStyles(theme => ({
    button: {
      margin: theme.spacing(1),
    },
}));

export default observer(
  class Review extends Component {

    async componentDidMount(){
        const endpoint = config.api + "/review";

        console.log("Calling");
        try {
            const reply = await fetch(endpoint, { method: "POST" }); 
            const result = await reply.json();
            await console.log(result);
        }
        catch{
            console.log("Error")
        }
    }
    sendData=()=>{
        this.props.model.reviewModel.PostReview();
    }

    handleDropDownChange = name => event => {
        console.log(event.target.value);
            this.props.model.reviewModel.setDesc(event.target.value);
      };

    handleTextInputChange = (myValue) => {
        console.log(myValue) 
    }
    render() {
      return (
        <div>
        <FormControl variant="filled">
            <InputLabel id="demo-simple-select-filled-label">Tutorials</InputLabel>
            <Select
                labelId="demo-simple-select-filled-label"
                id="demo-simple-select-filled"
                name="course"
            >
            <MenuItem value="">
                <em>None</em>
            </MenuItem>
            <MenuItem value={"ELL"} >English Language Learning </MenuItem>
            <MenuItem value={"CTTutor"}>Creative Technology Tutorials</MenuItem>
            <MenuItem value={"PNTutor"}>Practical Nursing</MenuItem>
            <MenuItem value={"Reboot"}>Reboot</MenuItem>
            </Select>

            <TextField 
            id="outlined-multiline-static"
            label="Feedback"
            multiline
            rows="5"
            variant="outlined"
            />
        </FormControl>
        <Button variant="contained" color="primary" size="small" className={useStyles.button} startIcon={<MailIcon />} onClick={()=>this.sendData()}> Send </Button>
          
        </div>
      );
    }
  }
);
