import React, { Component } from "react";
import { observer } from "mobx-react";
import { Link } from "react-router-dom";

import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TablePagination from "@material-ui/core/TablePagination";

import DisplayServiceRow from "../components/ServicesTable";
import TableRow from "@material-ui/core/TableRow";
import Grid from "@material-ui/core/Grid";
import SearchIcon from "@material-ui/icons/Search";
import TextField from "@material-ui/core/TextField";

///
/// This is the page that will set the fields that will appear when booking.
///
/// Props:
///     model: MainModel which holds every other model.
///
export default observer(
  class ServicesPage extends Component {
    async componentDidMount() {
      await this.props.model.serviceModel.getServicesData();
    }
    loadPreviousPage = () => {
      this.props.model.serviceModel.setPageNumber(-1);
    };
    loadNextPage = () => {
      this.props.model.serviceModel.setPageNumber(1);
    };
    handleChangeRowsPerPage = event => {
      this.props.model.serviceModel.setPageRow(event.target.value, 10);
    };
    findService = event => {
      if (event.target.value === "") {
        this.props.model.serviceModel.GetServiceData();
      }
      // this.props.model.serviceModel.setFindService(event.target.value);
    };

    render() {
      const canServices = this.props.model.getCanServices();
      const services = this.props.model.serviceModel.getServicesList();
      const page = this.props.model.serviceModel.getPageNumber();
      let row = this.props.model.serviceModel.getPageRow();
      let maxPage = Math.ceil(services.length / row);
      let fromp = 1;
      let top = 7;

      if (services.length < row) {
        maxPage = 1;
      }

      let max = false;
      let min = false;

      if (page < 2) min = true;
      else min = false;
      if (page >= Math.ceil(services.length / row)) max = true;
      else max = false;

      return (
        <div>
          <h1>Services</h1>
          {canServices && (
            <div>
              <Link to="/service">Add Service</Link>
            </div>
          )}
          <div>
            List of services here
            <TableRow>
              <TablePagination
                labelRowsPerPage="Row per page"
                labelDisplayedRows={({ from, to, count }) =>
                  `${page}-${maxPage}`
                }
                page={page}
                rowsPerPageOptions={[5, 10, 20, { label: "All", value: -1 }]}
                onChangeRowsPerPage={this.handleChangeRowsPerPage}
                backIconButtonProps={{
                  "aria-label": "Previous Page",
                  onClick: this.loadPreviousPage,
                  disabled: min
                }}
                nextIconButtonProps={{
                  "aria-label": "Next Page",
                  onClick: this.loadNextPage,
                  disabled: max
                }}
              ></TablePagination>
            </TableRow>
            <DisplayServiceRow model={this.props.model} />
            {/* <ul>
              {servicesArray.map(s => {
                //console.log(servicesArray);
                const url = "/service/" + s.getCode();
                return (
                  <li key={s.getOrder()}>
                    <Link to={url}>{s.getDesc()}</Link>
                  </li>
                );
              })}
            </ul> */}
            {/* <div>
              <Grid container spacing={1} alignItems="flex-end">
                <Grid item>
                  <SearchIcon />
                </Grid>
                <Grid item>
                  <TextField
                    id="input-with-icon-grid"
                    label="Find Service"
                    onChange={this.findService}
                  />
                </Grid>
              </Grid>
            </div> */}
          </div>
        </div>
      );
    }
  }
);
