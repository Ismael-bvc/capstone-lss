import React, { Component } from "react";
import { withRouter } from "react-router";
import PropTypes from "prop-types";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import { withStyles } from "@material-ui/core/styles";
import WalkInField from "../components/fields-walkin.js";
import axios from "axios";
import { observer } from "mobx-react";
import { TabContainer } from "react-bootstrap";
const qs = require("querystring");
const useStyles = makeStyles(theme => ({
  root: {
    "& > *": {
      margin: theme.spacing(3),
      top: theme.spacing(10),
      width: 500
    }
  }
}));

export default observer(
  class WalkInBook extends Component {
    // constructor(props) {
    //   super(props);
    //   this.state = {
    //     firstName: "",
    //     lastName: "",
    //     phone: "",
    //     email: "",
    //     course: ""
    //   };
    async componentDidMount() {
      const { sid } = this.props.match.params;
      await this.props.model.walkinModel.getServiceData(sid);
    }

    handleClick = e => {
      console.log(this.state);
      const config = {
        headers: {
          "Content-Type": "application/x-www-form-urlencoded"
        }
      };
      axios
        .post(
          " http://localhost:54321/booking",
          qs.stringify(this.state),
          config
        )
        .then(response => {
          console.log(response);
        })
        .catch(error => {
          console.log(error);
        });
      alert("Your booking has been submitted");
    };

    handleChangeText = e =>
      this.setState({
        [e.target.name]: e.target.value
      });

    render() {
      const code = this.props.model.walkinModel.getCode();
      const fieldsarray = this.props.model.walkinModel.getFields();
      // console.log(JSON.stringify(this.state));
      //
      // MATERIAL-UI REQUIREMENT #2: Needed for accessing "styles" CB function declared above this class.

      return (
        <div>
          <Typography align="center" variant="h4">
            {code} Booking
          </Typography>

          {fieldsarray.map(f => {
            // if (f.getRequired()) {
            //   return <li>{f.getCode()}</li>;
            // } else {
            //   return null;
            // }
            return <WalkInField model={f} key={f.getCode()}></WalkInField>;
          })}
        </div>

        // <div align="center">
        //   <Typography>
        //     <div>
        //       <div className={classes.root} noValidate autoComplete="off">
        //         <TextField
        //           id="standard-basic"
        //           onChange={this.handleChangeText}
        //           name="firstname"
        //           label="First Name"
        //         />
        //       </div>

        //       <div className={classes.root} noValidate autoComplete="off">
        //         <TextField
        //           id="standard-basic"
        //           onChange={this.handleChangeText}
        //           name="lastname"
        //           label="Last Name"
        //         />
        //       </div>

        //       <div className={classes.root} noValidate autoComplete="off">
        //         <TextField
        //           id="standard-basic"
        //           onChange={this.handleChangeText}
        //           name="phone"
        //           label="Phone"
        //         />
        //       </div>

        //       <div className={classes.root} noValidate autoComplete="off">
        //         <TextField
        //           id="standard-basic"
        //           onChange={this.handleChangeText}
        //           name="email"
        //           label="Email"
        //         />
        //       </div>

        //       <div className={classes.root} noValidate autoComplete="off">
        //         <TextField
        //           id="standard-basic"
        //           onChange={this.handleChangeText}
        //           name="course"
        //           label="Course"
        //         />
        //       </div>

        //       <div className={classes.root} noValidate autoComplete="off">
        //         <Button
        //           type="submit"
        //           onClick={this.handleClick}
        //           variant="outlined"
        //         >
        //           Submit
        //         </Button>
        //       </div>
        //     </div>
        //   </Typography>
        // </div>
      );
    }
  }
);

//
// MATERIAL-UI REQUIREMENT #3: I don't know why this is needed.
// WalkInBook.propTypes = {
//   classes: PropTypes.object.isRequired
// };
// export default withStyles(styles)(withRouter(WalkInBook));
