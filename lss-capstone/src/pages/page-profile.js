import React, { Component } from "react";
import avatar from "../images/avatar.png";
import { observer } from "mobx-react";

///Profile page

export default observer(
  class Profile extends Component {
    constructor(props) {
      super(props);
    }

    onClickUpdateUserName = e => {
      e.preventDefault();
      this.props.model.profile.updateUserName();
      // this.props.model.loginModel.resetPassword();
    };

    onClickChangePassword = e => {
      e.preventDefault();
      // this.props.model.profile.updateProfileInfo();
      this.props.model.profile.updatePassword();
    };

    userNameChange = e => {
      this.props.model.profile.setUpdatedUserName(e.target.value);
    };

    currentPasswordChange = e => {
      this.props.model.profile.setCurrentPassword(e.target.value);
    };

    newPasswordChange = e => {
      this.props.model.profile.setNewPassword(e.target.value);
    };

    confirmPasswordChange = e => {
      this.props.model.profile.setConfirmPassword(e.target.value);
    };

    componentDidMount() {
      this.props.model.profile.fetchUserDetails();
    }

    render() {
      const email = this.props.model.loginModel.getUsername();
      const updatedUserName = this.props.model.profile.getUpdatedUserName();
      const currentUserName = this.props.model.profile.getCurrentUserName();
      // const userName = currentUserName || updatedUserName;
      const userErrorMessage = this.props.model.profile.getUserErrorMessage();
      const passwordErrorMessage = this.props.model.profile.getPasswordErrorMessage();
      const newPassword = this.props.model.profile.getNewPassword();
      const confirmPassword = this.props.model.profile.getConfirmPassword();

      return (
        <div className="login-form">
          <h2>Profile</h2>
          <img src={avatar} alt={""} className="avatar" />
          <form>
            <div>
              <div>Username</div>
              <div>
                <input
                  type="text"
                  value={updatedUserName}
                  onChange={this.userNameChange}
                ></input>
              </div>
            </div>
            <div>
              <div> Email ID</div>
              <div>
                <input type="text" value={email}></input>
              </div>
            </div>
            {userErrorMessage ? (
              <div className="message-warning">{userErrorMessage}</div>
            ) : (
              <div></div>
            )}
            <div>
              <button onClick={this.onClickUpdateUserName}>
                Update User Name
              </button>
            </div>
          </form>

          <form onSubmit={this.onClickChangePassword}>
            <div>
              <div>New Password</div>
              <div>
                <input
                  type="password"
                  value={newPassword}
                  onChange={this.newPasswordChange}
                ></input>
              </div>
            </div>
            <div>
              <div>Confirm Password</div>
              <div>
                <input
                  type="password"
                  value={confirmPassword}
                  onChange={this.confirmPasswordChange}
                ></input>
              </div>
            </div>
            {passwordErrorMessage ? (
              <div className="message-warning">{passwordErrorMessage}</div>
            ) : (
              <div></div>
            )}
            <div>
              <button type="submit">Change Password</button>
            </div>
          </form>
        </div>
      );
    }
  }
);
