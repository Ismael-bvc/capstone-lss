import React, { Component } from "react";
import { observer } from "mobx-react";
import { Link } from "react-router-dom";

///
/// List of offered services.
///
/// Props:
///     model: MainModel which holds every other model.
///
export default observer(
  class OfferedServicesPage extends Component {
    render() {
      const canServices = this.props.model.getCanServices();
      
      return (
        <div className="page">
          <h2>Offered Services</h2>
            <div>
              List of services here (these should be buttons)
              <ul>
                <li><Link to="/WalkInBook">Creative Technologies Tutorials (walk-in)</Link></li>
                <li><Link to="/appointment">English Language Learnign (appointment)</Link></li>
              </ul>
            </div>
            <Link to="/">Home</Link>
        </div>
      );
    }
  }
);
