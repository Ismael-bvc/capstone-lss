import React, { Component } from "react";
import { observer } from "mobx-react";
import { Link } from "react-router-dom";
import { Service } from "../components/form-service.js";
import DisplayServiceRow from "../components/ServicesTable";

///
/// This is the page that will set the fields that will appear when booking.
///
/// Props:
///     model: MainModel which holds every other model.
///
export default observer(
  class ServicePage extends Component {
    async componentDidMount() {
      const { sid } = this.props.match.params;
      await this.props.model.serviceModel.getServiceData(sid);
    }

    render() {
      const canServices = this.props.model.getCanServices();
      const { sid } = this.props.match.params;
      const fieldsurl = "/servicefields/" + sid;
      const code = this.props.model.serviceModel.getCode();
      const desc = this.props.model.serviceModel.getDesc();
      const fieldsArray = this.props.model.serviceModel.getFields();

      return (
        <div>
          {canServices && (
            <div>
              <Link to="/services">Back to Services</Link>
            </div>
          )}
          {canServices && (
            <div>
              <Link to={fieldsurl}>Manage fields</Link>
            </div>
          )}

          <h2>{code}</h2>
          <h2>{desc}</h2>
          {/* <ul>
            {fieldsArray.map(f => {
              return (
                <li key={f.getOrder()}>
                  {f.getCode()} - {f.getDesc()} - {f.getMandatory()} -{" "}
                  {f.getType()} - {f.getLength()}
                </li>
              );
            })}
          </ul> */}

          <Service
            props={this.props}
            model={this.props.model}
            serviceName={desc}
            url={code}
          />
        </div>
      );
    }
  }
);
