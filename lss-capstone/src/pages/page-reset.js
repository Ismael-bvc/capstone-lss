import React, { Component } from "react";
import { observer } from "mobx-react";

import ResetPasswordForm from "../components/form-reset.js";
import ErrorPage from "./page-error.js";

///
/// This is the page for reseting password.
///
/// PROPS
///     @model: MainModel which holds every other model.
///
export default observer(
    class ResetPage extends Component {
        constructor() {
            super();

            this.state = { valid: false};
        }

        componentDidMount() {
            // Check token validity.
            const { sid } = this.props.match.params;
            const parts = sid.split('.');
            if (parts.length == 3) {
                const payload = JSON.parse(window.atob(parts[1]));
                if ((new Date()).getTime() < Number(payload.iat) + 600000) {
                    this.props.model.loginModel.reset();
                    this.props.model.loginModel.setUsername(payload.sub);
                    this.setState({valid:true});
                }
            }
        }

        goBack = () => {
            this.props.history.replace("/");
        }

        render() {
            const { sid } = this.props.match.params;
            const valid = this.state.valid;
            return (
                <div className="page">
                    {
                        valid
                        ? <ResetPasswordForm model={this.props.model} token={sid} goback={this.goBack} />
                        : <ErrorPage />
                    }
                </div>
            );
      }
    }
  );
  