import React, { Component } from "react";
import { observer } from "mobx-react";

import Typography from "@material-ui/core/Typography";

///
/// This is the error page.
///
/// PROPS
///     @model: MainModel which holds every other model.
///
export default observer(
  class ErrorPage extends Component {
    goBack = () => {
      this.props.history.replace("/");
    }

    render() {
      return (
        <div className="page">
            <h2>Uh-oh! Error!</h2>
            <div>
              Sorry! You are seeing this error page because of one of the following:
              <ul>
                <li>This resource is not accessible to you.</li>
                <li>This resource is not available.</li>
                <li>You have the wrong URL.</li>
                <li>A supernova occured in one corner of the galaxy, sending
                  particles travelling near the speed of light.</li>
              </ul>
              If you think that there is a mistake, please drop by N266 and inform us about it.
            </div>
            <button className="link-button" onClick={this.goBack}>Home</button>
        </div>
      );
    }
  }
);
