import React, { Component, Fragment } from "react";

import { observer } from "mobx-react";

import Typography from "@material-ui/core/Typography";

import DisplayTable from "../components/TutorWaitlistForCarousel";
import Container from "@material-ui/core/Container";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";

export default observer(
class Carousel extends Component {
  constructor(props) {
    super(props);
    this.state = {loading:true};
  }
  async componentDidMount() {
    this.props.model.displayModel.ChangeDisplay();
    await this.props.model.displayModel.GetCarousel();
    await this.setState({loading: false});
    clearInterval(this.refetch);
    this.refetch = setInterval(this.onTimerExpired.bind(this),5000);
  }

  async onTimerExpired(){
    this.props.model.displayModel.SetIndex();
    await this.props.model.displayModel.GetCarousel();
  }

  componentWillUnmount()
  {
    clearInterval(this.refetch);
  }

  render() {
    let render = <></>;
    let waitlistarray = this.props.model.displayModel.GetWaitListData();
    let desc = this.props.model.displayModel.GetdescData();
    if(waitlistarray !== "")
    {
      //console.log(waitlistarray);
      waitlistarray = JSON.parse(waitlistarray);
      render = <Fragment>
      <Typography variant="h3" align="center">
      Waitlist
      </Typography>
      <div className = "fader">
      <div align="center">{desc}</div>
      <div>
      <Container>
        <Table responsive striped hover size="sm">
          <TableHead>
            <TableRow>
              <TableCell>
                <Typography variant="h6">Queue</Typography>
              </TableCell>
              <TableCell>
                <Typography variant="h6">Name</Typography>
              </TableCell>
              <TableCell>
                <Typography variant="h6">Status</Typography>
              </TableCell>
            </TableRow>
          </TableHead>
        {
          waitlistarray.map((el, i) => {
            return <DisplayTable key={i} model={el} />
          })
        }
        </Table>
      </Container>
        </div>
        </div>
      </Fragment>
    }
    else
    {
      console.log("loading");
      render = <div>Loading...</div>;
    }

    return (
      <div>{render}</div>
    );
  }
});
