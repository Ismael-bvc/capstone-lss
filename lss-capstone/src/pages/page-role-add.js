import React, { Component } from "react";
import { observer } from "mobx-react";
import { AddRole } from "../components/form-addRole.js";

///
/// This is the page that will set the fields that will appear when booking.
///
/// PROPS
///     @model: MainModel which holds every other model.
///
export default observer(
  class AddRoles extends Component {
    render() {
      return (
        <div>
          <h1>Create a role</h1>
          <AddRole model={this.props.model} />
        </div>
      );
    }
  }
);
