import React, { Component } from "react";
import { observer } from "mobx-react";
import { Link } from "react-router-dom";

///
/// 
///
/// Props:
///     model: MainModel which holds every other model.
///
export default observer(
  class UserPage extends Component {
    async componentDidMount() {
      const {sid} = this.props.match.params;
      //await this.props.model.serviceModel.getServiceData(sid);
    }

    render() {
      const canUsers = this.props.model.getCanUsers();
      const { sid } = this.props.match.params;
      const scheduleurl = "/userschdule/" + sid;
      
      return (
        <div>
            {canUsers && (
                <div>
                    <Link to="/users">Back to Users</Link>
                </div>
            )}
            {canUsers && (
                <div>
                    <Link to={scheduleurl}>Manage schedule</Link>
                </div>
            )}
            <h2>{sid}</h2>
            
        </div>
      );
    }
  }
);
