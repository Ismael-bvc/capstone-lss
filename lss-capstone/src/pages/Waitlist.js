import React, { Component } from "react";

import { observer } from "mobx-react";

import TutorWaitlist from "../components/TutorWaitlist";

import Typography from "@material-ui/core/Typography";

///
/// PROPS
/// @model: should be the main model.
///
export default observer(
  class Waitlist extends Component {
    constructor(props) {
      super(props);

      this.state = {
        list: null
      };
    }

    //
    // Making a GET request using Manny's API.
    sampleCall = async () => {
      try {
        const endpoint = "http://localhost:54321/display";

        const fetchData = {
          method: "GET",
          headers: {
            "Content-Type": "application/json"
          }
        };

        const reply = await fetch(endpoint, fetchData);
        const data = await reply.json();

        console.log(data);
      } catch (e) {
        console.log(`From Waitlist ${e}`);
      }
    };

    async componentDidMount() {
      try {
        if (this.props.model.service) {
          let service = await this.props.model.getService().getWaitlist();
          this.setState({ list: service });
        }
      } catch (e) {
        console.log(`Waitlist page: Service does not exist -> ${e}`);
      }
    }

    render() {
      this.sampleCall();
      return (
        <div>
          <Typography variant="h3" align="center">
            Waitlist
          </Typography>
          <TutorWaitlist list={this.state.list} />
        </div>
      );
    }
  }
);
