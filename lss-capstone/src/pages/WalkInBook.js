import React, { Component } from "react";
import { withRouter } from "react-router";
import PropTypes from "prop-types";
import Typography from "@material-ui/core/Typography";
import { withStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import axios from "axios";
const qs = require("querystring");
const styles = theme => ({
  root: {
    "& > *": {
      margin: theme.spacing(3),
      bottom: theme.spacing(5),
      width: 400
    }
  }
});

class WalkInBook extends Component {
  constructor(props) {
    super(props);
    this.state = {
      firstName: "",
      lastName: "",
      phone: "",
      email: "",
      course: ""
    };
  }

  handleClick = e => {
    console.log(this.state);
    const config = {
      headers: {
        "Content-Type": "application/x-www-form-urlencoded"
      }
    };
    axios
      .post(" http://localhost:54321/booking", qs.stringify(this.state), config)
      .then(response => {
        console.log(response);
      })
      .catch(error => {
        console.log(error);
      });
    alert("Your booking has been submitted");
  };

  handleChangeText = e =>
    this.setState({
      [e.target.name]: e.target.value
    });

  render() {
    // console.log(JSON.stringify(this.state));
    //
    // MATERIAL-UI REQUIREMENT #2: Needed for accessing "styles" CB function declared above this class.
    const { classes } = this.props;
    return (
      <div align="center">
        <Typography>
          <div>
            <div className={classes.root} noValidate autoComplete="off">
              <TextField
                id="standard-basic"
                onChange={this.handleChangeText}
                name="firstname"
                label="First Name"
              />
            </div>

            <div className={classes.root} noValidate autoComplete="off">
              <TextField
                id="standard-basic"
                onChange={this.handleChangeText}
                name="lastname"
                label="Last Name"
              />
            </div>

            <div className={classes.root} noValidate autoComplete="off">
              <TextField
                id="standard-basic"
                onChange={this.handleChangeText}
                name="phone"
                label="Phone"
              />
            </div>

            <div className={classes.root} noValidate autoComplete="off">
              <TextField
                id="standard-basic"
                onChange={this.handleChangeText}
                name="email"
                label="Email"
              />
            </div>

            <div className={classes.root} noValidate autoComplete="off">
              <TextField
                id="standard-basic"
                onChange={this.handleChangeText}
                name="course"
                label="Course"
              />
            </div>

            <div className={classes.root} noValidate autoComplete="off">
              <Button
                type="submit"
                onClick={this.handleClick}
                variant="outlined"
              >
                Submit
              </Button>
            </div>
          </div>
        </Typography>
      </div>
    );
  }
}

//
// MATERIAL-UI REQUIREMENT #3: I don't know why this is needed.
WalkInBook.propTypes = {
  classes: PropTypes.object.isRequired
};
export default withStyles(styles)(withRouter(WalkInBook));
