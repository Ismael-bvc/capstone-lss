import React, { Component } from "react";
import { observer } from "mobx-react";
import { Link } from "react-router-dom";

import Typography from "@material-ui/core/Typography";

///
/// This is the page that will serve as "home" for the currently logged user.
///
/// PROPS
///     @model: MainModel which holds every other model.
///
export default observer(
  class Dashboard extends Component {
    render() {
      const canDashboard = this.props.model.getCanDashboard();
      const canList = this.props.model.getCanLists();

      return (
        <div>
          <h1>Dashboard</h1>
          {canDashboard && (
                <div>
                  <Link to="/waitlist">Waitlist</Link>
                </div>
            )}
            {canList && (
                <div>
                  <Link to="/prelist">Display List</Link>
                </div>
            )}
        </div>
      );
    }
  }
);
