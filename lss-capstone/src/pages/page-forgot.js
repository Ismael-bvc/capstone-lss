import React, { Component } from "react";
import { observer } from "mobx-react";

import ForgotPasswordForm from '../components/form-forgot.js';

///
/// This is the page for logging in. Will show up if user doesnt have token..
///
/// PROPS
///     @model: MainModel which holds every other model.
///
export default observer(
    class ForgotPage extends Component {
      goBack = () => {
        this.props.history.replace("/");
      }

      render() {
        return (
          <div className="page">
            <ForgotPasswordForm model={this.props.model} goback={this.goBack} />
          </div>
        );
      }
    }
  );
  