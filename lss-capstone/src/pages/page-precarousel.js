import React, { Component } from "react";

import { observer } from "mobx-react";

import Checkbox from "../components/CarouselCheckBoxes";

export default observer(
class PreCarousel extends Component {
  constructor(props) {
    super(props);
    //let services = this.props.model.getServices();
    //console.log( services+"========");
    //this.props.model.displayModel.Fillcourses(services);
  }
 moveToCarousel()
  {
    this.props.history.push("/list");
  }

  onTimerExpired =()=>{
    this.props.model.displayModel.SetIndex();
  }


  render() {
    return (
      <div variant="h3" align="center">
        <Checkbox onClick={()=>this.moveToCarousel()} props={this.props} num={this.props.model.displayModel.GetNumberOfCourses}/>
      </div>
    );
  }
});
