import React, { Component } from "react";
import { observer } from "mobx-react";
import { Link } from "react-router-dom";

import Typography from "@material-ui/core/Typography";

///
/// This is the page that will set the fields that will appear when booking.
///
/// PROPS
///     @model: MainModel which holds every other model.
///
export default observer(
  class ServiceFields extends Component {
    async componentDidMount() {
      const {sid} = this.props.match.params;
      await this.props.model.serviceModel.getServiceData(sid);
    }

    render() {
      const canServices = this.props.model.getCanServices();
      const { sid } = this.props.match.params;
      const fieldsurl = "/servicefields/" + sid;
      const viewurl = "/service/" + sid;
      const code = this.props.model.serviceModel.getCode();
      const desc = this.props.model.serviceModel.getDesc();
      const fieldsArray = this.props.model.serviceModel.getFields();
      return (
        <div>
          {canServices && (
            <div>
              <Link to="/services">Back to Services</Link>
            </div>
          )}
          {canServices && (
            <div>
              <Link to={viewurl}>View {sid}</Link>
            </div>
          )}
          <h1>Fields</h1>
          <h2>{desc}</h2>
          <h2>{code}</h2>
          <ul>
            {
              fieldsArray.map(f => {
                return <li key={f.getOrder()}>
                  {f.getCode()} - 
                  {f.getDesc()} - 
                  {f.getRequired()} - 
                  {f.getType()} - 
                  {f.getLength()} - 
                  {f.getDefault() ? "T" : "F"} -
                  {f.getEnabled() ? "T" : "F"}
                </li>
              })
            }
          </ul>
        </div>
      );
    }
  }
);
